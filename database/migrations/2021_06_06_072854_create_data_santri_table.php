<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataSantriTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('santri', function (Blueprint $table) {
            $table->id();
            $table->string("nama_santri");
            $table->text("alamat");
            $table->string("ttl");
            $table->integer("no_hp");
            $table->integer("no_hp_ortu");
            $table->string("jenis_kelamin");
            $table->string("angkatan_ppm");
            $table->string("kampus");
            $table->string("jurusan");
            $table->text("alamat_ortu");
            $table->string("email");
            $table->string("password");
            $table->string("nama_ayah");
            $table->string("profesi_ayah");
            $table->string("nama_ibu");
            $table->string("profesi_ibu");
            $table->string("dapukan_ortu");
            $table->integer("pendapatan_ortu");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('santri');
    }
}
