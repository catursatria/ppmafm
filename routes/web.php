<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('compro.home.index');
});

Route::get('/santri', function () {
    return view('admin.dashboard.index');
});


Route::get('/', 'ComproController@home')->name('compro.home');
Route::get('/contact-us', 'ComproController@contact')->name('compro.contact');

Route::get('/about-ppm-afm', function () {
    return view('compro.about.index');
})->name('compro.about');

Route::get('/team', function () {
    return view('compro.team.index');
})->name('compro.team');


Route::get('/coba', 'PercobaanController@index');
Route::resource('poin', 'Admin\PoinController');
Route::post('/poin/import_excel', 'PoinController@import_excel');

Route::get('/cobanih', 'CobaController@index');
Route::get("/santri/datasantri", 'DataSantriController@index')->name('santri.index');
Route::get("/santri/datasantri/create_data", 'DataSantriController@masukcreate');
Route::post("/santri/datasantri/create_data", 'DataSantriController@create')->name("santri.create");
Route::get("/santri/datasantri/{id}/edit", "DataSantriController@edit");
Route::post("/santri/datasantri/{id}/update", "DataSantriController@update")->name("santri.update");
Route::get("/santri/datasantri/{id}/delete", "DataSantriController@delete");
Route::post('/santri/datasantri/import_excel', 'DataSantriController@importexcel')->name('santri.import');

Route::get("/santri/datasantri/{id}/profile", "DataSantriController@profile");

Route::get('/berita','BeritaController@index');
Route::get('/buat','BeritaController@buat');
Route::POST('/berita/create','BeritaController@create');
Route::get('/berita/{id}/edit', 'BeritaController@edit');
Route::post('/berita/{id}/update','BeritaController@update');
Route::get('/berita/{id}/delete', 'BeritaController@delete' );
Route::get('/berita/{id}/view', 'BeritaController@view')->name('berita.show');
Route::get('/berita/list-berita','BeritaController@list')->name('berita.list');


