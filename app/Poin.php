<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Poin extends Model
{
    protected $table = 'afm_poin';

    protected $fillable = [
        'santri_id',
        'periode',
        'kbm',
        'kgt',
        'piket',
        'pu',
        'total_minus',
        'saldo_poin',
        'keterangan',
    ];

    public function santri()
    {
        return $this->belongsTo(Datasantri::class, 'santri_id');
    }
}

