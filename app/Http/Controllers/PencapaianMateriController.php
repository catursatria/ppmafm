<?php

namespace App\Http\Controllers;
use App\Models\Materi;
use Illuminate\Http\Request;

class PencapaianMateriController extends Controller
{
    public function index()
    {
        $data_siswa = App/Models/Materi::all();
        return view('siswa.index',['data_siswa'=>$data_siswa]);
        // return "ini list siswa";
    }

    public function create(Request $request)
    {
        dd($request);
        // \App\Models\Siswa::create($request->all());
        $siswa = new Siswa;
        $siswa->nama =$request->Nama;
        $siswa->keperluan =$request->Keperluan;
        $siswa->file =$request->File;
        $siswa->save();
        return redirect ('/siswa')->with('sukses','Data berhasil diinput');
    }

    public function edit ($id){
        $siswa = Siswa::find($id);
        return view('siswa/edit',['siswa'=> $siswa]);

    }

    public function update(Request $request, $id){
        $siswa = Siswa::find($id);
        $siswa->nama =$request->Nama;
        $siswa->keperluan =$request->Keperluan;
        $siswa->file=$request->File;
        $siswa->update();
        return redirect ('/siswa')->with('sukses','Data berhasil diinput');
    }

    public function delete($id){
        Siswa::find($id)->delete();
        // $siswa = Siswa::where('id',$id)->get();
        return redirect ('/siswa')->with('sukses1','Data berhasil dihapus');

    }
}
