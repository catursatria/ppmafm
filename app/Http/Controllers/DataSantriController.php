<?php

namespace App\Http\Controllers;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\SantriImport;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DataSantriController extends Controller
{
    public function index()
    {
        $data_santri = \App\Datasantri::all();
        return view('admin.dashboard.datasantri', ['data_santri' => $data_santri]);
    }

    public function masukcreate()
    {
        return view('admin.dashboard.createdatasantri');
    }

    public function create(Request $request)
    {

        $data = $request->all();

        $data['tanggal_lahir'] = Carbon::parse($request->tanggal_lahir)->format('Y-m-d');

        \App\Datasantri::create($data);

        return redirect("/santri/datasantri")->with('sukses', "Data berhasil masuk!");
    }

    public function edit($id)
    {
        $data_santri = \App\Datasantri::find($id);
        return view('admin.dashboard.editdatasantri', ['data_santri' => $data_santri]);
    }

    public function update(Request $request, $id)
    {
        // dd($request->all());
        $data_santri = \App\Datasantri::find($id);

        $date = Carbon::parse($request->tanggal_lahir)->format('Y-m-d');
        $data_santri->update([
            'nama_santri' => $request->nama_santri,
            'alamat' => $request->alamat,
            'tempat_lahir' => $request->tempat_lahir,
            'tanggal_lahir' => $date,
            'no_hp' => $request->no_hp,
            'no_hp_ortu' => $request->no_hp_ortu,
            'jenis_kelamin' => $request->jenis_kelamin,
            'angkatan' => $request->angkatan,
            'kampus' => $request->kampus,
            'fakultas' => $request->fakultas,
            'jurusan' => $request->jurusan,
            'alamat_ortu' => $request->alamat_ortu,
            'nama_ayah' => $request->nama_ayah,
            'profesi_ayah' => $request->profesi_ayah,
            'nama_ibu' => $request->nama_ibu,
            'profesi_ibu' => $request->profesi_ibu,
            'dapukan_ortu' => $request->dapukan_ortu,
            'pendapatan_ortu' => $request->pendapatan_ortu,
            ]);

        if($request->hasFile('avatar')){
            $request->file('avatar')->move('images/', $request->file('avatar')->getClientOriginalName());
            $data_santri->avatar = $request->file('avatar')->getClientOriginalName();
            $data_santri->save();
        }
        return redirect("/santri/datasantri")->with('sukses', "Data berhasil diupdate!");
    }

    public function delete($id)
    {
        $data_santri = \App\Datasantri::find($id);
        $data_santri->delete($data_santri);
        return redirect("/santri/datasantri")->with('sukses', "Data berhasil dihapus!");
    }

    public function profile($id)
    {
        $data_santri = \App\Datasantri::find($id);
        return view('admin.dashboard.profiledatasantri', ['data_santri' => $data_santri]);
    }

    public function importexcel(Request $request) 
    {
        // validasi
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);
     
        // menangkap file excel
        $file = $request->file('file');
     
        // membuat nama file unik
        $nama_file = rand().$file->getClientOriginalName();
     
        // upload ke folder file_siswa di dalam folder public
        $file->move('file_siswa',$nama_file);
     
        // import data
        Excel::import(new SantriImport, public_path('/file_siswa/'.$nama_file));
     
        // notifikasi dengan session
     
        // alihkan halaman kembali
        return redirect('/santri/datasantri')->with('sukses', 'Berhasil Import');
    }
}
