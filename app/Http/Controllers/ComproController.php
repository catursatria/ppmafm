<?php

namespace App\Http\Controllers;

use App\Berita;
use Illuminate\Http\Request;

class ComproController extends Controller
{
    public function home()
    {
        $news = Berita::orderby('created_at', 'desc')->paginate(5);
        return view('compro.home.index', compact(['news']));
    }

    public function contact()
    {
        return view('compro.contact.index');
    }
}
