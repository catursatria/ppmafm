<?php

namespace App\Http\Controllers\Admin;

use App\Poin;
use App\Datasantri;

use Maatwebsite\Excel\Facades\Excel;
use App\Imports\PoinImport;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Http\Request;

class PoinController extends Controller
{
    public function index()
    {
        if (request()->ajax()) {
            $query = Poin::with(['santri']);

            return DataTables::of($query)
            ->addColumn('action', function($item){
                return ' ';
            })
            ->rawColumns(['action'])
            ->make();

        }

        return view('admin.poin.index');

    }

    public function import_excel(Request $request)
    {

		// validasi
		$this->validate($request, [
			'file' => 'required|mimes:xlsx'
		]);

		// menangkap file excel
		$file = $request->file('file');

		// membuat nama file unik
		$nama_file = rand().$file->getClientOriginalName();

		// upload ke folder file_siswa di dalam folder public
		$file->move('file_poin',$nama_file);

		// import data
		Excel::import(new PoinImport, public_path('/file_poin/'.$nama_file));

		// alihkan halaman kembali
		return redirect()->route('poin.index')->with('success','Data Siswa Berhasil Diimport!');
    }
}
