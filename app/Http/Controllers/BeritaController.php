<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Carbon\Carbon;


class BeritaController extends Controller
{
    public function index()
    {
        $data_berita = \App\Berita::all();
        return view('admin.dashboard.news.berita', ['data_berita' => $data_berita]);
    }
    public function buat()
    {
        return view('admin.dashboard.news.buat');
    }
    public function create(request $request)
    {
        $berita = $request->all();
        $berita['tanggal_publish'] = Carbon::parse($request->tanggal_publish)->format('Y-m-d');
        $data = \App\Berita::create($berita);
       if($request->hasFile('foto')){
           $request->file('foto')->move('assets/dashboards/images/news/', $request->file('foto')->getClientOriginalName());
           $data->foto = $request->file('foto')->getClientOriginalName();
           $data->save();
        }
        return redirect('/berita')->with('sukses', 'Data berhasil ditambahkan');
    }
    public function edit($id)
    {
        $berita = \App\Berita::find($id);
        return view ('admin.dashboard.news.edit', ['berita'=>$berita]);
    }
    public function update(Request $request,$id)
    {
        $berita = $request->all();
        $berita["tanggal_publish"] = Carbon::parse($request->tanggal_publish)->format('Y-m-d');
        $data = \App\Berita::find($id);
        $data-> update($berita);
        if($request->hasFile('foto')){
            $request->file('foto')->move('assets/dashboards/images/news/', $request->file('foto')->getClientOriginalName());
            $data->foto = $request->file('foto')->getClientOriginalName();
            $data->save();
        }
        return redirect('/berita')->with('sukses','Berita berhasil diupdate');
    }
    public function delete($id)
    {
        $berita = \App\Berita::find($id);
        $berita->delete();
        return redirect('/berita')->with('sukses', 'Data berhasil dihapus');
    }

    public function view($id)
    {
        $berita = \App\Berita::find($id);
        //dd($berita);
        return view ('admin.dashboard.news.viewberita', compact('berita'));
    }
    public function list()
    {
        $berita = \App\Berita::orderby('created_at', 'desc')->paginate(15);
        return view ('admin.dashboard.news.list-berita', compact('berita'));
    }
}
