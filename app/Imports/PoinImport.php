<?php

namespace App\Imports;

use App\Poin;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\WithHeadingRow; //TAMBAHKAN CODE INI
use DateTime;
use Maatwebsite\Excel\Concerns\Importable;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\WithValidation;

class PoinImport implements ToModel, WithHeadingRow
{
    use Importable;
    /**
    * @param Collection $collection
    */
    public function model(array $row)
    {
        $now = new DateTime();

        if (!isset($row['santri_id'])) {
            return null;
        }
         Validator::make($row, [
            'santri_id' => 'unique:App\Karyawan,nik'
         ])->validate();

        return new Poin([
            'santri_id' => $row['santri_id'],
            'periode' => $row['periode'],
            'kbm' => $row['kbm'],
            'kgt' => $row['kgt'],
            'piket' => $row['piket'],
            'pu' => $row['pu'],
            'total_minus' => $row['total_minus'],
            'saldo_poin' => $row['saldo_poin'],
            'keterangan' => $row['keterangan'],
            'created_at'=>$now,
            'updated_at'=>$now,
        ]);
    }
}
