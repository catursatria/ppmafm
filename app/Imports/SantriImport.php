<?php

namespace App\Imports;

use App\Datasantri;
use Maatwebsite\Excel\Concerns\ToModel;
use Carbon\Carbon;

class SantriImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Datasantri([
            'nama_santri' => $row[0],
            'alamat' => $row[1],
            'tempat_lahir' => $row[2],
            'tanggal_lahir' => Carbon::parse($row[3])->format('Y-m-d'),
            'no_hp' => $row[4],
            'no_hp_ortu' => $row[5],
            'jenis_kelamin' => $row[6],
            'angkatan' => $row[7],
            'kampus' => $row[8],
            'fakultas' => $row[9],
            'jurusan' => $row[10],
            'alamat_ortu' => $row[11],
            'nama_ayah' => $row[12],
            'profesi_ayah' => $row[13],
            'nama_ibu' => $row[14],
            'profesi_ibu' => $row[15],
            'dapukan_ortu' => $row[16],
            'pendapatan_ortu' => $row[17],
            'pendapatan_ortu' => $row[17],
        ]);
    }
}
