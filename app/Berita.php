<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
#use Cviebrock\EloquentSluggable\Sluggable;

class Berita extends Model
{
   # use Sluggable;
    protected $dates = ['tanggal_publish'];
    protected $table = 'afm_berita';
    protected $fillable = ['judul','tanggal_publish','orientasi','isi_berita','sumber','foto'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     public function sluggable(): array
     {
         return [
             'slug' => [
                 'source' => 'judul'
                 ]
                ];
            }

            */
            public function foto()
            {
                if($this->foto){
                    return $this->foto;
                } else {
                    return asset('assets\dashboards\images\no-thumbnail.jpg');
                }
            }
}
