<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Datasantri extends Model
{
    protected $table = 'afm_santri';
    protected $fillable = ['nama_santri',
                           'alamat',
                           'tempat_lahir',
                           'tanggal_lahir',
                           'no_hp',
                           'no_hp_ortu',
                           'jenis_kelamin',
                           'angkatan',
                           'kampus',
                           'fakultas',
                           'jurusan',
                           'alamat_ortu',
                           'nama_ayah',
                           'profesi_ayah',
                           'nama_ibu',
                           'profesi_ibu',
                           'dapukan_ortu',
                           'pendapatan_ortu',
                           'avatar'];

    public function getAvatar()
    {
        if(!$this->avatar){
            return asset('images/default.jpg');
        }

        return asset('images/'.$this->avatar);
    }
    
}
