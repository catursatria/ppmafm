<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Materi extends Model
{
    protected $table = 'siswa';
    public $timestamp = false;
    protected $fillable = ['nama', 'keperluan', 'file'];
    use HasFactory;
}
