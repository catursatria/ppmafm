@extends('layouts.admin')
@section('title','Halaman Dashboard')
@push('custom-style')
<link href="{{asset('assets/dashboards/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
@endpush
@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="page-title mb-0 font-size-18">Dashboard</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Welcome to Qovex Dashboard</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->
<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h2 class="card-title mb-4"><strong>Tambah Data</strong></h2>
                <form class="outer-repeater" action="{{ route('santri.create') }}" method="POST">
                    @csrf
                    <div data-repeater-list="outer-group" class="outer">
                        <div data-repeater-item="" class="outer">
                            <div class="mb-3">
                                <label class="form-label" for="formname">Name</label>
                                <input type="text" name="nama_santri" class="form-control" id="formname" placeholder="Enter Santri's Name...">
                            </div>

                            <div class="mb-3">
                                <label for="formmessage">Alamat Santri</label>
                                <textarea id="formmessage" name="alamat" class="form-control" rows="3"></textarea>
                            </div>

                            {{-- <div class="mb-3">
                                <label class="form-label" for="formemail">Email :</label>
                                <input type="email" class="form-control" id="formemail" placeholder="Enter Santri's Email...">
                            </div> --}}

                            <div class="mb-3">
                                <label class="form-label" for="formname">Tempat Lahir</label>
                                <input type="text" class="form-control" name="tempat_lahir" id="formname" placeholder="Location">
                            </div>

                            <div class="mb-3">
                                <label class="form-label">Tanggal Lahir</label>
                                <div class="input-group" id="datepicker2">
                                    <input type="text" class="form-control" name="tanggal_lahir" placeholder="mm/dd/yyyy"
                                    data-date-container="#datepicker2" data-provide="datepicker">
                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>

                                <!-- input-group -->
                            </div>

                            <div class="mb-3">
                                <label class="form-label" for="formname">No. HP</label>
                                <input type="text" class="form-control" name="no_hp" id="formname" >
                            </div>

                            <div class="mb-3">
                                <label class="form-label" for="formname">No. HP Ortu Santri</label>
                                <input type="text" class="form-control" name="no_hp_ortu" id="formname" >
                            </div>

                            <div class="mb-3">
                                <label class="d-block mb-3">Jenis Kelamin</label>

                                <div class="form-check form-check-inline">
                                    <input type="radio" id="flexRadioDefault1" value="Laki-laki" name="jenis_kelamin" class="form-check-input">
                                    <label class="form-check-label" for="flexRadioDefault1">Laki-laki</label>
                                </div>

                                <div class="form-check form-check-inline">
                                    <input type="radio" id="flexRadioDefault2" value="Perempuan" name="jenis_kelamin" class="form-check-input">
                                    <label class="form-check-label" for="flexRadioDefault2">Perempuan</label>
                                </div>

                            </div>

                            <div class="mb-3">
                                <label class="form-label" for="formname">Angkatan</label>
                                <input type="text" class="form-control" name="angkatan" id="formname" placeholder="20xx">
                            </div>

                            <div class="mb-3">
                                <label class="form-label" for="formname">Kampus</label>
                                <input type="text" class="form-control" name="kampus" id="formname" placeholder="">
                            </div>

                            <div class="mb-3">
                                <label class="form-label" for="formname">Fakultas</label>
                                <input type="text" class="form-control" name="fakultas" id="formname" placeholder="">
                            </div>

                            <div class="mb-3">
                                <label class="form-label" for="formname">Jurusan</label>
                                <input type="text" class="form-control" name="jurusan" id="formname" placeholder="">
                            </div>

                            <div class="mb-3">
                                <label for="formmessage">Alamat Ortu Santri</label>
                                <textarea id="formmessage" class="form-control" name="alamat_ortu" rows="3"></textarea>
                            </div>

                            <div class="mb-3">
                                <label class="form-label" for="formname">Nama Ayah Santri</label>
                                <input type="text" class="form-control" name="nama_ayah" id="formname" placeholder= "">
                            </div>

                            <div class="mb-3">
                                <label class="form-label" for="formname">Profesi Ayah Santri</label>
                                <input type="text" class="form-control" name="profesi_ayah" id="formname" placeholder="">
                            </div>

                            <div class="mb-3">
                                <label class="form-label" for="formname">Nama Ibu Santri</label>
                                <input type="text" class="form-control" name="nama_ibu" id="formname" placeholder= "">
                            </div>

                            <div class="mb-3">
                                <label class="form-label" for="formname">Profesi Ibu Santri</label>
                                <input type="text" class="form-control" name="profesi_ibu" id="formname" placeholder="">
                            </div>

                            <div class="mb-3">
                                <label class="form-label" for="formname">Dapukan Ortu (Ayah/Ibu)</label>
                                <input type="text" class="form-control" name="dapukan_ortu" id="formname" placeholder= "">
                            </div>

                            <div class="mb-3">
                                <label class="form-label" for="formname">Pendapatan Ortu (Rupiah)</label>
                                <input type="text" class="form-control" name="pendapatan_ortu" id="formname" placeholder="">
                            </div>

                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- end row -->
@stop

@push('custom-script')
    <script src="{{asset('assets/dashboards/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
@endpush
