@extends('layouts.admin')
@section('title','Halaman Dashboard')
@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="page-title mb-0 font-size-18">Dashboard</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Welcome to Qovex Dashboard</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->
@if (session('sukses'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        {{ session('sukses') }}
    </div>
@endif


<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <a href="/santri/datasantri/create_data"><button type="button" class="btn btn-outline-primary waves-effect waves-light">Tambah Data</button></a>
                <!-- Button trigger import -->
                <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">Import Data</button>
                <div class="table-responsive border">

                    <table class="table table-hover table-responsive">

                        <thead>
                            <tr>
                                <th>Nama Santri</th>
                                <th>Alamat</th>
                                <th>Tempat lahir</th>
                                <th>Tanggal lahir</th>
                                <th>No. HP</th>
                                <th>No. HP Ortu</th>
                                <th>Jenis Kelamin</th>
                                <th>Angkatan</th>
                                <th>Kampus</th>
                                <th>Fakultas</th>
                                <th>Jurusan</th>
                                <th>Alamat Ortu</th>
                                <th>Nama Ayah</th>
                                <th>Profesi Ayah</th>
                                <th>Nama Ibu</th>
                                <th>Profesi Ibu</th>
                                <th>Dapukan Ortu (Ayah/Ibu)</th>
                                <th>Pendapatan Ortu (Rupiah)</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        @foreach ($data_santri as $santri)
                        <tbody>
                            <tr>
                                <td><a href="/santri/datasantri/{{$santri->id}}/profile">{{$santri->nama_santri}}</a></td>
                                <td>{{$santri->alamat}}</td>
                                <td>{{$santri->tempat_lahir}}</td>
                                <td>{{$santri->tanggal_lahir}}</td>
                                <td>{{$santri->no_hp}}</td>
                                <td>{{$santri->no_hp_ortu}}</td>
                                <td>{{$santri->jenis_kelamin}}</td>
                                <td>{{$santri->angkatan}}</td>
                                <td>{{$santri->kampus}}</td>
                                <td>{{$santri->fakultas}}</td>
                                <td>{{$santri->jurusan}}</td>
                                <td>{{$santri->alamat_ortu}}</td>
                                <td>{{$santri->nama_ayah}}</td>
                                <td>{{$santri->profesi_ayah}}</td>
                                <td>{{$santri->nama_ibu}}</td>
                                <td>{{$santri->profesi_ibu}}</td>
                                <td>{{$santri->dapukan_ortu}}</td>
                                <td>{{$santri->pendapatan_ortu}}</td>
                                <td>
                                    <a href="/santri/datasantri/{{$santri->id}}/edit" class="btn btn-outline-warning waves-effect waves-light">Edit</a>
                                    <a href="/santri/datasantri/{{$santri->id}}/delete" class="btn btn-outline-danger waves-effect waves-light" onclick="return confirm('Yakin mau dihapus?')">Delete</a>
                                </td>
                            </tr>
                        </tbody>
                        @endforeach
                    </table>
                </div>
                <!-- Import Excel -->
                <div class="modal fade" id="exampleModal">
                    <div class="modal-dialog">
                        <form method="post" action="{{ route('santri.import') }}" enctype="multipart/form-data">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
                                </div>
                                <div class="modal-body">
                         
                                    {{ csrf_field() }}
                         
                                    <label>Pilih file excel</label>
                                    <div class="form-group">
                                        <input type="file" name="file" required="required">
                                    </div>
                         
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Import</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- end row -->
@stop
