@extends('layouts.app')
@section('title', 'Berita')
@section('content')
<main>
    <!-- main content wrapper -->
    <div class="content-wrapper">
        <section class="content-block">
            <div class="container">
                <div class="row mt-5">
                    <div class="col-lg-12 less-wide">
                        <div class="blog-holder">
                            <article class="blog-article">
                                <div class="blog-title text-center pb-5">
                                    <h1>{{ $berita->judul }}</h1>
                                    <div class="blog-lists border-0">
                                        <ul class="blog-list-items">
                                            <li>By: <a href="#">Admin</a></li>
                                            <li>
                                                {{ $berita->tanggal_publish->format('D,d M Y') }}
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="blog-desc pt-5">
                                    <div class="blog-img">
                                        <div class="image-wrap">
                                            <figure class="">
                                                @if($berita->foto)<img src="{{asset('assets/dashboards/images/news/' . $berita->foto()) }}"> @endif
                                                {{-- <img src="{{asset('assets/dashboards/images/news/narkoba.jpg')}}" > --}}
                                            </figure>
                                        </div>
                                    </div>
                                    <div>
                                      <p>  {{ $berita->sumber }} -- {{ $berita->orientasi }} </p>
                                    </div>
                                    <div>
                                        {!! $berita->isi_berita !!}
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
        </section>
        </div>
        <!--/main content wrapper -->
</main>
@stop
