@extends('layouts.admin')
@section('title', 'Berita PPM AFM')
@section('content')

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="page-title mb-0 font-size-18">Berita PPM</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item active">Welcome to Qovex Dashboard</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
    @if (session('sukses'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <i class="mdi mdi-check-all me-2"></i> {{ session('sukses') }}
        </div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <a href="/buat">
                        <button type="button" class="btn btn-primary btn-lg btn-block waves-effect waves-light mb-1">
                            Tambah Berita
                        </button>
                        <style>
                            .btn-primary {
                                Float: right;
                            }
                            </style>
                    </a>
                    <!-- Table-->
                    <table class="table table-hover mb-0">
                        <thead>
                            <tr>
                                <th>Judul</th>
                                <th>Tanggal Publish</th>
                                <th>Orientasi Berita</th>
                                <th>Isi Berita</th>
                                <th>Sumber</th>
                                <th>Foto</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($data_berita as $berita)
                                <tr>
                                    <td>{{ $berita->judul }}</td>
                                    <td>{{ $berita->tanggal_publish }}</td>
                                    <td>{{ $berita->orientasi }}</td>
                                    <td>{!! $berita->isi_berita !!}</td>
                                    <td>{{ $berita->sumber }}</td>
                                    <td>@if ($berita->foto)
                                        <img src="{{asset('assets/dashboards/images/news/' . $berita->foto()) }}" height="45"></td>
                                        @endif
                                    <td>
                                        <a href="/berita/{{$berita->id}}/edit" class="btn btn-warning btn-rounded waves-effect waves-light btn-sm">Edit</a>
                                        <a href="/berita/{{$berita->id}}/delete" class="btn btn-danger btn-rounded waves-effect waves-light btn-sm" onclick="return confirm('Berita akan hilang, anda yakin?')">Hapus</a>
                                        <a href="/berita/{{$berita->id}}/view" class="btn btn-info btn-rounded waves-effect waves-light btn-sm" >View</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <!--/Table-->
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->
@stop


