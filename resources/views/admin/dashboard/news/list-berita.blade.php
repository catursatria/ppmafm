@extends('layouts.app')
@section('title', 'List Berita')
@section('content')
<main>
    <section class="visual">
        <div class="visual-inner blog-default-banner dark-overlay parallax" data-stellar-background-ratio="0.55">
            <div class="container">
                <div class="visual-text-large text-left visual-center">
                    <h1 class="visual-title visual-sub-title">Portal Berita PPM AFM</h1>
                    <p>Jelajahi berita dan info terbaru PPM AFM disini!</p>
                    <div class="breadcrumb-block">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"> Home </a></li>
                            {{-- <li class="breadcrumb-item"><a href="index.html"> Blog </a></li> --}}
                            <li class="breadcrumb-item active"> Portal Berita </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="content-wrapper">
        <section class="content-block">
            <div class="container">
                <div class="row multiple-row">
                    @foreach ( $berita as $item )
                    <div class="col-md-6 col-lg-4">
                        <div class="col-wrap">
                            <div class="post-grid reverse-grid">
                                <div class="img-block post-img">
                                    <a href="{{ route('berita.show',$item->id) }}"><img src="{{asset('assets/dashboards/images/news/' . $item->foto()) }}" alt="images"></a>
                                    <time class="post-date" datetime="2016-10-10">{{ $item->tanggal_publish->format('d M Y') }}</time>
                                </div>
                                <div class="post-text-block bg-gray-light">
                                    <strong class="content-title mb-0"><a href="#">{{ $item->judul }}</a></strong>
                                    <span class="content-sub-title">{{ $item->tanggal_publish->format('D,d M Y') }}</span>
                                    <p>{{ $item->orientasi }}</p>
                                    <div class="post-meta clearfix">
                                        <div class="post-link-holder">
                                            <a href="{{ route('berita.show',$item->id) }}">Read Story <span class="fa fa-arrow-right"><span
                                                        class="sr-only">&nbsp;</span></span></a>
                                        </div>
                                        {{-- <div class="post-social text-right">
                                            <ul class="social-network social-small">
                                                <li><a href="#"><span class="icon-facebook"><span
                                                                class="sr-only">&nbsp;</span></span></a></li>
                                                <li><a href="#"><span class="icon-twitter"><span
                                                                class="sr-only">&nbsp;</span></span></a></li>
                                            </ul>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="btn-container full-width-btn top-space">
                    {{-- <a href="javascript:void(0)" class="btn btn-black">LOAD MORE<span class="c-ripple js-ripple"><span --}}
                                {{-- class="c-ripple__circle"></span></span></a> --}}

                                {{ $berita->links() }}
                </div>
            </div>
        </section>
    </div>

</main>
@stop
