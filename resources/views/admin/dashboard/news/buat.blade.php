@extends('layouts.admin')
@section('title', 'Buat Berita')
    @push('custom-style')
        <!--
                                                                        <link href="{{ asset('assets/dashboards/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}"
                                                                            rel="stylesheet">
                                                                        <style>
                                                                            .ck-editor__editable_inline {
                                                                    min-height: 500px;
                                                                }
                                                                        </style>
                                                                    -->
    @endpush
@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h4 class="page-title mb-0 font-size-18">Buat Berita</h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item active">Welcome to Qovex Dashboard</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <form class="outer-repeater" action="/berita/create" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="card-body">
                        <div data-repeater-list="outer-group" class="outer">
                            <div data-repeater-item class="outer">
                                <div class="mb-3">
                                    <label class="form-label" for="formname">Judul</label>
                                    <input autocomplete="off" name="judul" type="text" class="form-control" id="formname"
                                        placeholder="Masukkan Judul...">
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Tanggal Publish</label>
                                    <div class="input-group" id="datepicker2">
                                        <input autocomplete="off" name="tanggal_publish" type="text" class="form-control"
                                            placeholder="mm/dd/yyyy" data-date-container='#datepicker2'
                                            data-provide="datepicker" data-date-autoclose="true">
                                        <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                    </div>
                                    <!-- input-group -->
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Orientasi Berita</label>
                                    <div>
                                        <textarea name="orientasi" required class="form-control" rows="5"
                                            placeholder="Permulaan Berita..."></textarea>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label">Isi Berita</label>
                                    <div>
                                        <textarea id="elm1" name="isi_berita" class="form-control"
                                            rows="5"></textarea>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label class="form-label" for="formname">Sumber Berita</label>
                                    <input autocomplete="off" name="sumber" type="text" class="form-control" id="formname"
                                        placeholder="Contoh : Liputan6.com,Jakarta">
                                </div>
                                <h4 class="card-title">Foto</h4>
                                <div class="input-group">
                                    <input name="foto" type="file" class="form-control" id="inputGroupFile02">
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary right">
                        Selesai
                    </button>
                </form>
            </div>
        </div>
    </div>

    <!-- end row -->
@stop


@push('custom-script')
    <script src="{{ asset('assets/dashboards/js/pages/form-editor.init.js') }}"></script>
    <script src="{{ asset('assets/dashboards/libs/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('assets/dashboards/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
@endpush
