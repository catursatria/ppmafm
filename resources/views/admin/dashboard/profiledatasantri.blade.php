@extends('layouts.admin')
@section('title','Halaman Dashboard')
@section('content')

<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="page-title mb-0 font-size-18">Profile</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Pages</a></li>
                    <li class="breadcrumb-item active">Profile</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->

<div class="main-content">

    <div class="page-content">
        <!-- start row -->
        <div class="row">
            <div class="col-md-12 col-xl-3">
                <div class="card">
                    <div class="card-body">
                        <div class="profile-widgets py-3">

                            <div class="text-center">
                                <div class="">
                                    <img src="{{$data_santri->getAvatar()}}" class="avatar-lg mx-auto img-thumbnail rounded-circle">

                                </div>

                                <div class="mt-3 ">
                                    <a href="#" class="text-dark fw-medium font-size-20">{{$data_santri->nama_santri}}</a>

                                    <p class="text-body mt-1 mb-1 font-size-15 ">{{$data_santri->kampus}}</p>

                                    <span class="badge bg-info font-size-15">{{$data_santri->angkatan}}</span>

                                </div>

                                <div class="row p-1">
                                    <div class="col-md-6">
                                        <h6 class="text-muted">
                                            Fakultas:
                                        </h6>

                                    </div>

                                    <div class="col-md-6">
                                        <h6 class="text-muted" style="text-align: right">
                                            {{$data_santri->fakultas}}
                                        </h6>

                                    </div>
                                </div>

                                <div class="row p-1">
                                    <div class="col-md-6">
                                        <h6 class="text-muted">
                                            Jurusan:
                                        </h6>

                                    </div>

                                    <div class="col-md-6">
                                        <h6 class="text-muted" style="text-align: right">
                                            {{$data_santri->jurusan}}
                                        </h6>

                                    </div>
                                </div>

                                <div class="mt-4">

                                    <a href="/santri/datasantri/{{$data_santri->id}}/edit" class="btn btn-outline-warning waves-effect waves-light btn-block">Edit Profile</a>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title mb-3">Personal Information</h5>

                        <div class="mt-3">
                            <p class="font-size-12 text-muted mb-1">Jenis Kelamin</p>
                            <h6 class="">{{$data_santri->jenis_kelamin}}</h6>
                        </div>

                        <div class="mt-3">
                            <p class="font-size-12 text-muted mb-1">Tempat, Tanggal Lahir</p>
                            <h6 class="">{{$data_santri->tempat_lahir}}, {{date('d-m-Y', strtotime($data_santri->tanggal_lahir))}}</h6>
                        </div>

                        <div class="mt-3">
                            <p class="font-size-12 text-muted mb-1">Phone number</p>
                            <h6 class="">{{$data_santri->no_hp}}</h6>
                        </div>

                        <div class="mt-3">
                            <p class="font-size-12 text-muted mb-1">Alamat Rumah</p>
                            <h6 class="">{{$data_santri->alamat}}</h6>
                        </div>

                        {{-- <div class="mt-3">
                            <p class="font-size-12 text-muted mb-1">Email Address</p>
                            <h6 class="">StaceyTLopez@armyspy.com</h6>
                        </div> --}}


                    </div>
                </div>

                <div class="card">
                    <div class="card-body">
                        {{-- <h5 class="card-title mb-2">Daftar Dapukan</h5> --}}
                        <a href="/santri/datasantri/" class="btn btn-outline-info waves-effect waves-light btn-block">Data Santri</a>
                    </div>
                </div>

            </div>

            <div class="col-md-12 col-xl-9">
                <div class="row">
                    <div class="col-md-12 col-xl-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center">
                                    <div class="col-8">
                                        <p class="mb-2">Point Santri</p>
                                        <h4 class="mb-0">...</h4>
                                    </div>
                                    <div class="col-4">
                                        <div class="text-end">

                                            <div class="progress progress-sm mt-3">
                                                <div class="progress-bar" role="progressbar" style="width: 30%"
                                                    aria-valuenow="62" aria-valuemin="0" aria-valuemax="100">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xl-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="row align-items-center">
                                    <div class="col-8">
                                        <p class="mb-2">Kuantitas Materi</p>
                                        <h4 class="mb-0">%...</h4>
                                    </div>
                                    <div class="col-4">
                                        <div class="text-end">

                                            <div class="progress progress-sm mt-3">
                                                <div class="progress-bar bg-warning" role="progressbar"
                                                    style="width: 78%" aria-valuenow="78" aria-valuemin="0"
                                                    aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>

                <div class="card">
                    <div class="card-body">

                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs nav-tabs-custom nav-justified" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-bs-toggle="tab" href="#daftar_dapukan" role="tab">
                                    <span class="d-block d-sm-none"><i class="fas fa-home"></i></span>
                                    <span class="d-none d-sm-block">Daftar Dapukan</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-bs-toggle="tab" href="#data_orang_tua" role="tab">
                                    <span class="d-none d-sm-block">Data Orang Tua</span>
                                </a>
                            </li>
                            {{-- <li class="nav-item">
                                <a class="nav-link" data-bs-toggle="tab" href="#settings" role="tab">
                                    <span class="d-block d-sm-none"><i class="far fa-envelope"></i></span>
                                    <span class="d-none d-sm-block">Settings</span>
                                </a>
                            </li> --}}
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content p-3 text-muted">
                            {{-- TAB PANE DAFTAR DAPUKAN --}}
                            <div class="tab-pane active" id="daftar_dapukan" role="tabpanel">
                                <div class="timeline-count mt-5">
                                    <!-- Timeline row Start -->
                                    <div class="row">

                                        <!-- Timeline 1 -->
                                        <div class="timeline-box col-lg-4">
                                            <div class="mb-5 mb-lg-0">
                                                <div class="item-lable bg-primary rounded">
                                                    <p class="text-center text-white">2016 - 20</p>
                                                </div>
                                                <div class="timeline-line active">
                                                    <div class="dot bg-primary"></div>
                                                </div>
                                                <div class="vertical-line">
                                                    <div class="wrapper-line bg-light"></div>
                                                </div>
                                                <div class="bg-light p-4 rounded mx-3">
                                                    <h5>Back end Developer</h5>
                                                    <p class="text-muted mt-1 mb-0">Voluptatem accntium
                                                        doemque lantium, totam rem aperiam, eaque ipsa quae
                                                        ab illo quasi sunt explicabo.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Timeline 1 -->

                                        <!-- Timeline 2 -->
                                        <div class="timeline-box col-lg-4">
                                            <div class="mb-5 mb-lg-0">
                                                <div class="item-lable bg-primary rounded">
                                                    <p class="text-center text-white">2013 - 16</p>
                                                </div>
                                                <div class="timeline-line active">
                                                    <div class="dot bg-primary"></div>
                                                </div>
                                                <div class="vertical-line">
                                                    <div class="wrapper-line bg-light"></div>
                                                </div>
                                                <div class="bg-light p-4 rounded mx-3">
                                                    <h5>Front end Developer</h5>
                                                    <p class="text-muted mt-1 mb-0">Vivamus ultrices massa
                                                        tellus, sed convallis urna interdum eu. Pellentesque
                                                        htant morbi varius mollis et quis nisi.</p>

                                                </div>
                                            </div>
                                        </div>
                                        <!-- Timeline 2 -->
                                        <!-- Timeline 2 -->
                                        <div class="timeline-box col-lg-4">
                                            <div class="mb-5 mb-lg-0">
                                                <div class="item-lable bg-primary rounded">
                                                    <p class="text-center text-white">2013 - 16</p>
                                                </div>
                                                <div class="timeline-line active">
                                                    <div class="dot bg-primary"></div>
                                                </div>
                                                <div class="vertical-line">
                                                    <div class="wrapper-line bg-light"></div>
                                                </div>
                                                <div class="bg-light p-4 rounded mx-3">
                                                    <h5>Front end Developer</h5>
                                                    <p class="text-muted mt-1 mb-0">Vivamus ultrices massa
                                                        tellus, sed convallis urna interdum eu. Pellentesque
                                                        htant morbi varius mollis et quis nisi.</p>

                                                </div>
                                            </div>
                                        </div>
                                        <!-- Timeline 2 -->

                                        <!-- Timeline 3 -->
                                        <div class="timeline-box col-lg-4">
                                            <div class="mb-5 mb-lg-0">
                                                <div class="item-lable bg-primary rounded">
                                                    <p class="text-center text-white">2011 - 13</p>
                                                </div>
                                                <div class="timeline-line active">
                                                    <div class="dot bg-primary"></div>
                                                </div>
                                                <div class="vertical-line">
                                                    <div class="wrapper-line bg-light"></div>
                                                </div>
                                                <div class="bg-light p-4 rounded mx-3">
                                                    <h5>UI /UX Designer</h5>
                                                    <p class="text-muted mt-1 mb-0">Suspendisse potenti.
                                                        senec netus malesuada fames ac turpis egesta vitae
                                                        blandit ac tempus nulla.</p>

                                                </div>
                                            </div>
                                        </div>
                                        <!-- Timeline 3 -->
                                    </div>
                                    <!-- Timeline row Over -->

                                </div>
                            </div>

                            {{-- TAB PANE DATA ORANG TUA --}}
                            <div class="tab-pane" id="data_orang_tua" role="tabpanel">
                                <div id="revenue-chart" class="apex-charts mt-4">
                                    <div class="table-responsive">
                                        <table class="table table-centered mb-0">
                                            <thead>
                                                <tr>
                                                    <th scope="col"></th>
                                                    <th scope="col">Nama</th>
                                                    <th scope="col">Profesi</th>
                                                    <th scope="col">Dapukan</th>
                                                    <th scope="col">Penghasilan</th>
                                                    {{-- <th scope="col" colspan="2">Payment Status</th> --}}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th>Ayah</th>
                                                    <td>{{$data_santri->nama_ayah}}</td>
                                                    <td>{{$data_santri->profesi_ayah}}</td>
                                                    <td>{{$data_santri->dapukan_ortu}}</td>
                                                    <td>{{$data_santri->pendapatan_ortu}}</td>
                                                </tr>

                                                <tr>
                                                    <th>Ibu</th>
                                                    <td>{{$data_santri->nama_ibu}}</td>
                                                    <td>{{$data_santri->profesi_ibu}}</td>
                                                    <td>{{$data_santri->dapukan_ortu}}</td>
                                                    <td>{{$data_santri->pendapatan_ortu}}</td>
                                                </tr>



                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>


            </div>


        </div>

        <!-- end row -->

    </div>

    <!-- End Page-content -->


</div>
<!-- end main content-->
<!-- end row -->
@stop

