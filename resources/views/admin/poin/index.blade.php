@extends('layouts.admin')
@section('title','Data Poin Santri')
@section('content')
<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="page-title mb-0 font-size-18">Data Poin Santri</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item active">Welcome to Qovex Dashboard</li>
                </ol>
            </div>

        </div>
    </div>
</div>
<!-- end page title -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

            </div>
        </div>
    </div>
    <!-- end col -->
</div>

<!-- end row -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                {{-- <h4 class="card-title">Default Datatable</h4>
                <p class="card-title-desc">DataTables has most features enabled by default, so all
                    you need to do to use it with your own tables is to call the construction
                    function: <code>$().DataTable();</code>.
                </p> --}}

                <table id="datatable" class="table table-bordered dt-responsive nowrap"
                    style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Periode</th>
                            <th>KBM</th>
                            <th>KGT</th>
                            <th>Piket</th>
                            <th>PU</th>
                            <th>Total Minus</th>
                            <th>Saldo Poin</th>
                            <th>Keterangan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>

            </div>
        </div>
    </div>
    <!-- end col -->
</div>

<!-- end row -->
@stop

@push('custom-script')
<script>
    var datatable = $('#datatable').DataTable({
        processing: true,
        serverSide: true,
        ordering: true,
        ajax: {
            url: '{!! url()->current() !!}',
        },
        columns: [
            {
                data: 'santri.nama_santri',
                name: 'santri.nama_santri'
            },
            {
                data: 'periode',
                name: 'periode'
            },
            {
                data: 'kbm',
                name: 'kbm'
            },
            {
                data: 'kgt',
                name: 'kgt'
            },
            {
                data: 'piket',
                name: 'piket'
            },
            {
                data: 'pu',
                name: 'pu'
            },
            {
                data: 'total_minus',
                name: 'total_minus'
            },
            {
                data: 'saldo_poin',
                name: 'saldo_poin'
            },
            {
                data: 'keterangan',
                name: 'keterangan'
            },
            {
                data: 'action',
                name: 'action',
                orderable: false,
                searcable: false,
                width: '15%'
            },
        ]
    })

</script>
@endpush

