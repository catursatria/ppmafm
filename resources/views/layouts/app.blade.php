<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>PPM AFM - @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Company Profile PPM AFM" name="description" />
    <meta content="</code.jar>" name="author" />

    @include('includes.apps.style')
    @stack('custom-style')
  </head>
  <body class="white-overlay">
    <div class="preloader" id="pageLoad">
        <div class="holder">
            <div class="coffee_cup"></div>
        </div>
    </div>

    <!-- main wrapper -->
    <div id="wrapper" class="no-overflow-x">
        <div class="page-wrapper">
            <!-- ========== Header Start ========== -->
            @include('includes.apps.header')
            <!-- ========== Header End ========== -->
            <main>
                @yield('content')
            </main>
        </div>
        <!-- footer of the page -->
        @include('includes.apps.footer')
        <!--/footer of the page -->
    </div>


    {{-- Script --}}
    @include('includes.apps.script')
    @stack('custom-script')
  </body>
</html>
