<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>PPM AFM - @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Dashboard PPM AFM" name="description" />
    <meta content="</code.jar>" name="author" />

    @include('includes.admin.style')
    @stack('custom-style')
  </head>
  <body data-layout="detached" data-topbar="colored">
    <div class="container-fluid">
        <!-- Begin page -->
        <div id="layout-wrapper">
            <!-- ========== Header Start ========== -->
            @include('includes.admin.header')
            <!-- ========== Header End ========== -->
            <!-- ========== Left Sidebar Start ========== -->
            @include('includes.admin.sidebar')
            <!-- ========== Left Sidebar End ========== -->

            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <!-- Start Page-content -->
                <div class="page-content">
                    @yield('content')
                </div>
                <!-- End Page-content -->

                {{-- Footer --}}
                @include('includes.admin.footer')
            </div>
            <!-- end main content-->
        </div>
        <!-- END layout-wrapper -->
    </div>
    <!-- end container-fluid -->


    {{-- Script --}}
    @include('includes.admin.script')
    @stack('custom-script')
  </body>
</html>
