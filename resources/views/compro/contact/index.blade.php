@extends('layouts.app')
@section('title', 'Kontak PPM AFM')
@push('custom-style')
    <!-- Icons css-->
    <link href="{{ asset('assets/compro/web-fonts/icons.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/compro/web-fonts/font-awesome/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/compro/web-fonts/plugin.css') }}" rel="stylesheet" />

@endpush
@section('content')

{{-- <main class="no-banner"> --}}
    <!-- visual/banner of the page -->
    <section class="visual">
        <div class="visual-inner contact-banner dark-overlay parallax" data-stellar-background-ratio="0.55">
            <div class="centered">
                <div class="container">
                    <div class="visual-text visual-center">
                        <h1 class="visual-title visual-sub-title">Contact Us</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/visual/banner of the page -->
    <!-- main content wrapper -->
    <div class="content-wrapper">
        <section class="content-block pb-0">
            <div class="container">
                <div class="contact-container">
                    <h6 class="content-title contact-title">GET IN TOUCH WITH US</h6>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="info-slot">
                                <div class="icon"><span class="custom-icon-map-marker"></span></div>
                                <div class="text">
                                    <address>707 London Road, Isleworth
                                        <br>Middlesex, TW7 7QD
                                        <br>United Kingdom</address>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="info-slot">
                                <div class="icon"><span class="custom-icon-headset"></span></div>
                                <div class="text">
                                    <ul class="content-list contact-list">
                                        <li><span class="label-text">HELPLINE</span> <a href="tel:02078777777">(020) 7877 7777</a></li>
                                        <li><span class="label-text">ENQUIRIE</span> <a href="tel:02078777777">(020) 7877 7777</a></li>
                                        <li><span class="label-text">FAX</span> <a href="tel:02078777777">(020) 7877 7777</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="info-slot">
                                <div class="icon"><span class="custom-icon-message"></span></div>
                                <div class="text">
                                    <ul class="content-list contact-list">
                                        <li><a href="mailto:support@roxine-online">support@roxine-online</a></li>
                                        <li><a href="mailto:info@roxine-online.com">info@roxine-online.com</a></li>
                                        <li><a href="mailto:help@roxine-online.com">help@roxine-online.com</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="map-holder embed-responsive-21by9 grayscaled-map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15860.988260601527!2d106.8325864!3d-6.3620617!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1b817da416ece8e7!2sPondok%20Pesantren%20Mahasiswa%20%22PPM%22%20Al%20Faqih%20Mandiri%20Depok!5e0!3m2!1sid!2sid!4v1628528512140!5m2!1sid!2sid" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            {{-- <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2485.0605547231917!2d-0.33554378354576586!3d51.47540277962994!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48760cf99dee6c25%3A0xc9d2780dbbcbb933!2sLondon+Rd%2C+Isleworth+TW7%2C+UK!5e0!3m2!1sen!2snp!4v1473394388477" height="450" style="border:0"></iframe> --}}
        </div>
        <aside class="content-block">
            <div class="container">
                <h3>Nearest Campus</h3>
                <div class="logo-container">
                    <div class="owl-carousel logo-slide" id="waituk-owl-slide-4">
                        <div class="slide-item">
                            <img src="{{ asset('assets/compro/img/compro/logo-ui.png') }}" alt="images description">
                        </div>
                        <div class="slide-item">
                            <img src="{{ asset('assets/compro/img/compro/logo-pnj.png') }}" alt="images description">
                        </div>
                        <div class="slide-item">
                            <img src="{{ asset('assets/compro/img/compro/logo-ug.png') }}" alt="images description">
                        </div>
                        <div class="slide-item">
                            <img src="{{ asset('assets/compro/img/compro/logo-up.png') }}" alt="images description">
                        </div>
                        <div class="slide-item">
                            <img src="{{ asset('assets/compro/img/compro/logo-ubsi.png') }}" alt="images description">
                        </div>
                        <div class="slide-item">
                            <img src="{{ asset('assets/compro/img/compro/logo-unindra.png') }}" alt="images description">
                        </div>
                        <div class="slide-item">
                            <img src="{{ asset('assets/compro/img/compro/logo-aka.png') }}" alt="images description">
                        </div>
                        <div class="slide-item">
                            <img src="{{ asset('assets/compro/img/compro/logo-polmed.png') }}" alt="images description">
                        </div>
                    </div>
                </div>
            </div>
        </aside>
    </div>
    <!--/main content wrapper -->
{{-- </main> --}}
@stop
