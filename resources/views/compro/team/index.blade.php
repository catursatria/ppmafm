@extends('layouts.app')
@section('title','Team')

@section('content')
<div id="wrapper">
    <div class="page-wrapper">
        <main>
            <!-- visual/banner of the page -->
            {{-- <section class="visual">
                <div class="visual-inner about-banner dark-overlay parallax" data-stellar-background-ratio="0.55">
                    <div class="centered">
                        <div class="container">
                            <div class="visual-text visual-center">
                                <h1 class="visual-title visual-sub-title">About Business</h1>
                                <div class="breadcrumb-block">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="index.html"> Home </a></li>
                                        <li class="breadcrumb-item active"> About Company </li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section> --}}

            <div class="banner banner-home">
                <!-- revolution slider starts -->
                <div id="rev_slider_279_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="restaurant-header" style="margin:0px auto;background-color:#fff;padding:0px;margin-top:0px;margin-bottom:0px;">
                    <div id="rev_slider_70_1" class="rev_slider fullscreenabanner" style="display:none;" data-version="5.1.4">
                        <ul>
                            <li class="slider-color-schema-dark" data-index="rs-2" data-transition="fade" data-slotamount="7" data-easein="default" data-easeout="default" data-masterspeed="1000" data-rotate="0" data-saveperformance="off" data-title="Slide" data-description="">
                                <!-- main image for revolution slider -->
                                <img src="{{ asset('assets/compro/img/compro/team.jpg') }}" alt="image description" data-bgposition="center center" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-bgfit="cover" data-no-retina>
                                <div class="tp-caption tp-shape tp-shapewrapper" id="slide-1699-layer-10" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-width="full"
                                data-height="full" data-whitespace="nowrap" data-type="shape" data-basealign="slide" data-responsive_offset="on" data-responsive="off" data-frames='[{"from":"y:0;sX:1;sY:1;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                                data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="background-color:rgba(0, 0, 0, 0.57);"> </div>
                                <div class="slider-main-title text-white tp-caption tp-resizeme rs-parallaxlevel-1" data-x="['left','left','left','left']" data-hoffset="['0','0','0','0']" data-y="['top','top','middle','middle']" data-voffset="['250','150','50','50']" data-width="['1200','960','720','540']" data-textalign="center" data-fontsize="['100','88','64','48']" data-fontweight="900" data-letterspacing="['10','8','5','0']" data-lineheight="['184','100','72','60']" data-height="none" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:0px;" data-mask_out="x:inherit;y:inherit;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-paddingright="[25,25,25,25]" data-paddingleft="[25,25,25,25]">
                                    OUR TEAM
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--/visual/banner of the page -->
            <!-- main content wrapper -->
            <div class="content-wrapper">
                <section class="content-block">
                    <div class="container text-center">
                        <div class="heading bottom-space">
                            <h2>Alim Faqih.<span> Akhlaqul Karimah.</span> Mandiri.</h2>
                        </div>
                        <div class="description">
                            <p>Delightful unreserved impossible few estimating men favourable see entreaties. She propriety immediate was improving. He or entrance humoured likewise moderate. Much nor game son say feel. Fat make met can must form into gate. Me we offending prevailed discovery. </p>
                        </div>
                    </div>
                </section>
                <section class="content-block p-0">
                    <div class="container-fluid">
                        <div class="content-slot alternate-block">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="bg-stretch img-wrap wow slideInLeft">
                                        <img src="{{ asset('assets/compro/img/compro/ketua.jpg') }}" style="width: 12%" class="image_team" alt="ppmafm">
                                    </div>
                                </div>
                                <div class="col col-lg-6">
                                    <div class="text-wrap">
                                        <h3>Ketua Pondok Pesantren Mahasiswa</h3>
                                        <h4>H. Dedi Rinaldi S.T., M.M </h4>
                                        <p>“Jadilah bagian dari generus penerus yang profesional religius sarjana yang mubaligh mubaligh yang sarjana”</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="bg-stretch img-wrap wow slideInRight">
                                        <img src="{{ asset('assets/compro/img/compro/degur.jpg') }}" style="width: 12%" class="image_team" alt="ppmafm">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="text-wrap">
                                        <h3>Dewan Guru</h3>
                                        <h4>Ketua:</h4>
                                        <p>Ust. Muhammad Bachrudin</p>
                                        <h4>Anggota:</h4>
                                        <p> Ust. Muhammad Sulthon Aulia,
                                            Ust. Anji Hidayat,
                                            Ust. Prakash Faqih Arifin,
                                            Ustz. Ade Citra Puspasari</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="bg-stretch img-wrap wow slideInLeft">
                                        <img src="{{ asset('assets/compro/img/compro/pamong.jpg') }}" style="width: 18%" alt="ppmafm">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="text-wrap">
                                        <h3>Pamong</h3>
                                        <p>Guru Pamong  bertugas mendampingi dewan guru/instruktur PPG dalam membimbing santri pada kegiatan pengembangan perangkat pembelajaran dan PPL (sesuai jadwal pelaksanaan PPG) dan kurikulum. </p>
                                        <h4>Ketua:</h4>
                                        <p>Nisfulaili Abdurrohman</p>
                                        <h4>Anggota:</h4>
                                        <p>Khoirul Umam,
                                            Catur Satria,
                                            Muhammad Fajri Assalam,
                                            Nivia Nurbayani,
                                            Sefi Aulia,
                                            Irma Gayatri,
                                            Ananda Sabiila,
                                            Viola Septia Irfanda,
                                            Annisa Faradilla Ulfa,
                                            Maulida Rohmatul Fadhiila
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="bg-stretch img-wrap wow slideInLeft">
                                        <img src="{{ asset('assets/compro/img/compro/kesiswaan.jpg') }}" style="width: 12%" class="image_team" alt="ppmafm">
                                    </div>
                                </div>
                                <div class="col col-lg-6">
                                    <div class="text-wrap">
                                        <h3>Kesiswaan</h3>
                                        <p>Sebagai wahana bagi santri ppm AFM untuk mengembangkan diri seoptimal mungkin, baik yang berkenaan dengan segi-segi individualitasnya, segi sosialnya, segi aspirasinya, segi kebutuhannya dan segi potensi santri yang lainnya.</p>
                                        <h4>Ketua:</h4>
                                        <p>Faisal Amir Maz</p>
                                        <h4>Wakil Ketua:</h4>
                                        <p>Unggul Yudanira</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="bg-stretch img-wrap wow slideInRight">
                                        <img src="{{ asset('assets/compro/img/compro/keput.jpg') }}" style="width: 12%" class="image_team" alt="ppmafm">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="text-wrap">
                                        <h3>Keputrian</h3>
                                        <p>sebagai sarana untuk menambah wawasan keilmuan dalam bidang keagamaan bagi santriwati khususnya yang terdapat di ppm AFM. Selain itu untuk menanamkan nilai-nilai Islam pada akhlak santriwati dalam bersikap dan bertingkah laku dalam kehidupan sehari-hari</p>
                                        <h4>Ketua:</h4>
                                        <p>Unggul Yudanira</p>
                                        <h4>Wakil Ketua:</h4>
                                        <p>Nivia Nurbayani</p>
                                        <h4>Anggota:</h4>
                                        <p>Risma Rosalia,
                                            Yuri,
                                            Lola
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="bg-stretch img-wrap wow slideInLeft">
                                        <img src="{{ asset('assets/compro/img/compro/sekre.jpg') }}" style="width: 12%" class="image_team" alt="ppmafm">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="text-wrap">
                                        <h3>Sekretaris</h3>
                                        <p>Mengurus hal-hal yang berhubungan kesekretariatan dan administrasi. membantu dan mengurusi pendataan santri PPM AFM</p>
                                        <h4>Ketua:</h4>
                                        <p>Prasetyo</p>
                                        <h4>Anggota:</h4>
                                        <p>Mikael Alfian, Rizqy Qowiyu, Helma Justia Feyruzi</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="bg-stretch img-wrap wow slideInLeft">
                                        <img src="{{ asset('assets/compro/img/compro/ku.jpg') }}" style="width: 12%" class="image_team" alt="ppmafm">
                                    </div>
                                </div>
                                <div class="col col-lg-6">
                                    <div class="text-wrap">
                                        <h3>Keuangan Umum</h3>
                                        <p>Bertanggung jawab atas segala aktivitas yang berhubungan dengan keuangan dan bertindak sebagai koodinasi keuangan ppm, membuat anggaran </p>
                                        <h4>Ketua:</h4>
                                        <p>Agung Rizky</p>
                                        <h4>Anggota:</h4>
                                        <p>Bagus Kuncoro, Pandu Tri W., Andre Royan, Hegina Salsahabila, Novi Yusfita</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="bg-stretch img-wrap wow slideInRight">
                                        <img src="{{ asset('assets/compro/img/compro/kurkul.jpg') }}" style="width: 12%" class="image_team" alt="ppmafm">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="text-wrap" >
                                        <h3>Kurikulum</h3>
                                        <p>Sarana dalam mengukur kemampuan diri dan konsumsi pendidikan. Berkaitan dengan pencapaian target yang membuat santri menjadi mudah memahami berbagai materi. Serta mampu melaksanakan proses pembelajaran setiap harinya dengan mudah. </p>
                                        <h4>Ketua:</h4>
                                        <p>Givon Fatakhul</p>
                                        <h4>Anggota:</h4>
                                        <p>Jordan Fadilah,
                                        Raden Adhianto Leksono,
                                        Laverda Raul,
                                        Nida Setianingrum,
                                        Jannie Aldriana,
                                        Alifah Azka Nisrina,
                                        Annisa Aufa Dina,
                                        Sekar Adelia Putri
                                             </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="bg-stretch img-wrap wow slideInLeft">
                                        <img src="{{ asset('assets/compro/img/compro/humas.jpg') }}" style="width: 12%" class="image_team" alt="ppmafm">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="text-wrap">
                                        <h3>Humas</h3>
                                        <p>Menjalin komunikasi dengan masyarakat ppm, mengelola penyampaian dan penyebaran informasi terkait kegiatan, pengumuman dsb. Divisi yang membuat konsep publikasi, sehingga setiap kegiatan dan acara yang direncanakan dapat diketahui dengan baik oleh khalayak </p>
                                        <h4>Ketua:</h4>
                                        <p>Unggul Yudinara</p>
                                        <h4>Anggota:</h4>
                                        <p>Indra Gunawan, Raisa Nur Afifah, Salma Sita Rosulina </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="bg-stretch img-wrap wow slideInLeft">
                                        <img src="{{ asset('assets/compro/img/compro/kreatif.jpg') }}" style="width: 12%" class="image_team" alt="ppmafm">
                                    </div>
                                </div>
                                <div class="col col-lg-6">
                                    <div class="text-wrap">
                                        <h3>Kreatif</h3>
                                        <p>Menjadi wadah atau ide kreatif publikasi yang dibutuhkan oleh setiap kegiatan divisi di PPM dalam bentuk desain digital, video, dan audio. Bertanggung jawab untuk menghasilkan ide-ide yang menarik perhatian dan Bertanggung Jawab dalam acara yang terdapat di PPM</p>
                                        <h4>Ketua:</h4>
                                        <p>Naufal Abdullah Fawas Kamal </p>
                                        <h4>Anggota:</h4>
                                        <p>Marsa Nur Alifah, Bilqis Royyan, Muhammad Faisal Majid, Indah Apriliani, Said Arina Hendra, Salwa Rubia Darussalam, Sania Rizki Maharani, Bian Nursolichin, Alifya Addina, Dita Hasifa Asmarawati, Nadiva Regita Anjani</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="bg-stretch img-wrap wow slideInRight">
                                        <img src="{{ asset('assets/compro/img/compro/timtam.jpg') }}" style="width: 12%" class="image_team" alt="ppmafm">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="text-wrap">
                                        <h3>Tim Tamu</h3>
                                        <p>Melayani dan menerima tamu yang berkunjung ke PPM </p>
                                        <h4>Ketua:</h4>
                                        <p>Muhammad Rizki A. S. </p>
                                        <h4>Anggota:</h4>
                                        <p>David Aldani, Wildan Fathurizqi, Dwi Suci, Mita Novela, Putik, Isnaini Meianna</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="bg-stretch img-wrap wow slideInLeft">
                                        <img src="{{ asset('assets/compro/img/compro/sarpras.jpg') }}" style="width: 12%" class="image_team" alt="ppmafm">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="text-wrap">
                                        <h3>Sarana Prasarana</h3>
                                        <p>Melengkapi sarana dan prasaran yang dibutuhkan PPM, melaksanakan pelayanan umum, pembinaan urusan dalam, pengadaan, pembekalan/melengkapi dan pemeliharaan serta invetarisasi materiil di lingkungan PPM.  </p>
                                        <h4>Ketua:</h4>
                                        <p>Bagus Dewanto  </p>
                                        <h4>Anggota:</h4>
                                        <p>Alhazmi, Mustajib, Laverda Raul, Hanna Sabila Yasaro, Salsabila Minarbika, Alma Tiara Cantika</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="bg-stretch img-wrap wow slideInLeft">
                                        <img src="{{ asset('assets/compro/img/compro/divkes.jpg') }}" style="width: 12%" class="image_team" alt="ppmafm">
                                    </div>
                                </div>
                                <div class="col col-lg-6">
                                    <div class="text-wrap">
                                        <h3>Divisi Kesehatan</h3>
                                        <p>Melengkapi sarana dan prasarana Kesehatan, melengkapi dan melayani kebutuhan santri terhadap obat-obatan, mendata dan mengurusi  santri yang sakit, merawat serta melayani kebutuhan santri yang sakit</p>
                                        <h4>Ketua:</h4>
                                        <p>Bukhori Fathullah </p>
                                        <h4>Anggota:</h4>
                                        <p>Alfay Anshorulloh Majid, Iqbal Firmansyah, Amanda Safira, Maulida Rohmatul Fadhilla, Lola Miftahul, Salsabila Iza</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="bg-stretch img-wrap wow slideInRight">
                                        <img src="{{ asset('assets/compro/img/compro/me.jpg') }}" style="width: 12%" class="image_team" alt="ppmafm">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="text-wrap">
                                        <h3>Mechanical Engineer</h3>
                                        <p>Membantu merawat fasilitas yang ada (Gedung, alat-alat dsb.), membantu memperbaiki jika ada fasilitas yang rusak, usaha mandiri dengan adanya AFM Service, AFM Furniture</p>
                                        <h4>Ketua:</h4>
                                        <p>Fuad Mudzakir</p>
                                        <h4>Anggota:</h4>
                                        <p>Bagus Dewanto, Muhammad Faisal Majid, Alfay Anshorulloh Majid, Iqbal Firmansyah, Sefi Aulia, Laksmita</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="bg-stretch img-wrap wow slideInLeft">
                                        <img src="{{ asset('assets/compro/img/compro/dapur.jpg') }}" style="width: 12%" class="image_team" alt="ppmafm">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="text-wrap">
                                        <h3>Dapur</h3>
                                        <p>Mensejahterakan seluruh warga PPM AFM dalam hal kebutuhan pangan, penghubung tim masak dengan santri </p>
                                        <h4>Ketua:</h4>
                                        <p>Sultan Aris Arramadhan</p>
                                        <h4>Anggota:</h4>
                                        <p>Malik, Dimas Al Malik Aziz, Denis Al Malik Aziz, Tengku  F. A, Claresta Aptarini, Shita Laila </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="bg-stretch img-wrap wow slideInLeft">
                                        <img src="{{ asset('assets/compro/img/compro/ubank.jpg') }}" style="width: 12%" class="image_team" alt="ppmafm">
                                    </div>
                                </div>
                                <div class="col col-lg-6">
                                    <div class="text-wrap">
                                        <h3>Ubank</h3>
                                        <p>Sebagai penyediaan kebutuhan santri PPM AFM </p>
                                        <h4>Ketua:</h4>
                                        <p>Rudi Salam W. P.</p>
                                        <h4>Anggota:</h4>
                                        <p>Ahmad Faqih, Risma Rosalia, Riska Khoiruningrum, Nurul Akhyuri, Febilkis Noor Rahma, Dara Melinda Hepta, Bulan Kartika Maharani</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="bg-stretch img-wrap wow slideInRight">
                                        <img src="{{ asset('assets/compro/img/compro/psdm.jpg') }}" style="width: 12%" class="image_team" alt="ppmafm">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="text-wrap">
                                        <h3>Pengembangan Sumber Daya Muslim</h3>
                                        <p>Memperlancar kinerja divisi-divisi PPM AFM dengan membantu dan membimbing dalam efisiensi divisi baik secara kualitas maupun kuantitas, menjulurkan tangan kepala divisi yang kurang lancar, membuat kegiatan yang dapat mengembangkan sumber daya Santriwan dan Santriwati PPM AFM. </p>
                                        <h4>Ketua:</h4>
                                        <p>Reffi Ramadhan</p>
                                        <h4>Anggota:</h4>
                                        <p>Fauzan Andri, Raisa Nur Afifah, Hayuning Widiastuti, Shita Laila</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="bg-stretch img-wrap wow slideInLeft">
                                        <img src="{{ asset('assets/compro/img/compro/fr.jpg') }}" style="width: 12%" class="image_team" alt="ppmafm">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="text-wrap">
                                        <h3>Fund Raising</h3>
                                        <p>Mencari pendanaan untuk acara-acara yang ada di PPM AFM, membuat paid promote, dengan membuka usaha diberbagai bidang misalnya yang terdapat di PPM yaitu AFM Laudry dan usaha lainnya.  </p>
                                        <h4>Ketua:</h4>
                                        <p>Rayhan Janatama</p>
                                        <h4>Anggota:</h4>
                                        <p>Muhammad Anwar Hadid P. H, Gina Hasanah,  Laksmita, Azka Dini Yuntari</p>
                                    </div>
                                </div>
                            </div>
                            <section class="content-block">

                            </section>
                        </div>
                    </div>
                </section>
            </div>
            <!--/main content wrapper -->
        </main>
    </div>
</div>
@stop

