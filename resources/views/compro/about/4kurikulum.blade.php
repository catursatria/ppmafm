<section class="content-block">
    <div class="container">
        <div class="block-heading bottom-space">
            {{-- <h3 class="block-top-heading">THE LIFE SIZE</h3> --}}
            <h2 class="block-main-heading">Kurikulum</h2>
            <span class="block-sub-heading">View multiple content block layouts under features section.</span>
            <div class="divider"><img src="img/divider.png" alt="images description"></div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="bottom-space-small-only">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magnaal qua. Ut enim ad minim veniam, quis nostrud exercitation ulla mco laboris nisi ut aliquip ex ea commodo consequat.</p>
                    <p>Excepteur sint occaecat cupidatat non proident, sunt in culp a qui officia deserunt mollit anim id est laborum. Sed ut per spiciatis unde omnis iste natus error sit voluptatem accusan tium doloremque laudantium, totam rem aperiam, eaque ip </p>
                    <div class="btn-container top-m-space">
                        <a href="#" class="btn btn-trans-gray">VIEW PROJECTS</a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="bottom-s-space">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <ul class="content-links">
                            <li><a href="#"> WEB & INTERACTIVE DESIGN</a></li>
                            <li><a href="#">CONTENT MANAGEMENT</a></li>
                            <li><a href="#">WEB APPLICATIONS</a></li>
                            <li><a href="#">SOFTWARE DEVELOPMENT</a></li>
                            <li><a href="#">ECOMMERCE SOLUTIONS</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-6">
                        <ul class="content-links">
                            <li><a href="#">DATABASE DESIGN</a></li>
                            <li><a href="#">TECHNICAL DOCUMENTATION</a></li>
                            <li><a href="#">SOFTWARE ARCHITECTURE</a></li>
                            <li><a href="#">SEARCH OPTIMIZATION</a></li>
                            <li><a href="#">WEB PROMOTIONS</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
