@extends('layouts.app')
@section('title', 'Tentang PPM AFM')
@push('custom-style')
    <!-- Icons css-->
    <link href="{{ asset('assets/compro/web-fonts/icons.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/compro/web-fonts/font-awesome/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/compro/web-fonts/plugin.css') }}" rel="stylesheet" />

@endpush
@section('content')
    <!-- visual/banner of the page -->
    <section class="visual">
        <div class="visual-inner about-banner dark-overlay parallax" data-stellar-background-ratio="0.55">
            <div class="centered">
                <div class="container">
                    <div class="visual-text visual-center">
                        <h1 class="visual-title visual-sub-title">About PPM AFM</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--/visual/banner of the page -->
    <!-- main content wrapper -->
    <div class="content-wrapper">
        @include('compro.about.1tentangafm')
        @include('compro.about.2visimisi')
        <section class="content-block quotation-block quotation-block-v7 black-overlay-6 parallax" data-stellar-background-ratio="0.55">
            <div class="container">
                <div class="inner-wrapper">
                    <div class="block-heading text-right">
                        {{-- <h3 class="block-top-heading text-white">Quotes</h3> --}}
                        <h3 class="block-main-heading text-white">Life is pain, but you can choose what type.
                            Either the pain on the road to success. Or the pain of being haunted with regret. Choose wisely
                        </h3>
                        <span class="block-sub-heading text-white">Sourav Verma</span>
                    </div>
                </div>
            </div>
        </section>
        @include('compro.about.3foto')
        {{-- @include('compro.about.4kurikulum') --}}
        @include('compro.about.5kompetensikelulusan')
        <aside class="content-block">
            <div class="container">
                <h2>Nearest Campus</h2>
                <div class="logo-container">
                    <div class="owl-carousel logo-slide" id="waituk-owl-slide-4">
                        <div class="slide-item">
                            <img src="{{ asset('assets/compro/img/compro/logo-ui.png') }}" alt="images description">
                        </div>
                        <div class="slide-item">
                            <img src="{{ asset('assets/compro/img/compro/logo-pnj.png') }}" alt="images description">
                        </div>
                        <div class="slide-item">
                            <img src="{{ asset('assets/compro/img/compro/logo-ug.png') }}" alt="images description">
                        </div>
                        <div class="slide-item">
                            <img src="{{ asset('assets/compro/img/compro/logo-up.png') }}" alt="images description">
                        </div>
                        <div class="slide-item">
                            <img src="{{ asset('assets/compro/img/compro/logo-ubsi.png') }}" alt="images description">
                        </div>
                        <div class="slide-item">
                            <img src="{{ asset('assets/compro/img/compro/logo-unindra.png') }}" alt="images description">
                        </div>
                        <div class="slide-item">
                            <img src="{{ asset('assets/compro/img/compro/logo-aka.png') }}" alt="images description">
                        </div>
                        <div class="slide-item">
                            <img src="{{ asset('assets/compro/img/compro/logo-polmed.png') }}" alt="images description">
                        </div>
                    </div>
                </div>
            </div>
        </aside>
    </div>
    <!--/main content wrapper -->
@stop
