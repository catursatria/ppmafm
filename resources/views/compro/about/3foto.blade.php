<section class="content-block portfolio-block" id="container">
    <div class="bottom-space text-center text-uppercase">
        <h2>Fasilitas</h2>
    </div>
    {{-- <ul class="filter-nav text-center button-group filter-button-group">
        <li>
            <button data-filter="*">NGAJI</button>
        </li>
        <li>
            <button data-filter=".ui">AMAL SHOLIH</button>
        </li>
        <li>
            <button data-filter=".programming">NASIHAT</button>
        </li>
        <li>
            <button data-filter=".photography">DOA MALAM</button>
        </li>
        <li>
            <button data-filter=".ecommerce">EVENT</button>
        </li>
    </ul> --}}
    <div class="row">
        <div class="gallery-item col-lg-3 col-md-3 ui photography">
            <figure class="picture-item img-block shine-effect image-zoom port-v2">
                <img src="{{ asset('assets/compro/img/compro/home-banner-2.jpg') }}" alt="images description">
                <figcaption>
                    <div class="link-box">
                        <h4 style="color: whitesmoke">Masjid</h4>
                    </div>
                </figcaption>
            </figure>
        </div>
        <div class="gallery-item col-lg-3 col-md-3 photography programming">
            <figure class="picture-item img-block shine-effect image-zoom port-v2">
                <img src="{{ asset('assets/compro/img/compro/home-banner-2.jpg') }}" alt="images description">
                <figcaption>
                    <div class="link-box">
                        <h4 style="color: whitesmoke">Gedung Putra</h4>
                    </div>
                </figcaption>
            </figure>
        </div>
        <div class="gallery-item col-lg-3 col-md-3 ui photography">
            <figure class="picture-item img-block shine-effect image-zoom port-v2">
                <img src="{{ asset('assets/compro/img/compro/home-banner-2.jpg') }}" alt="images description">
                <figcaption>
                    <div class="link-box">
                        <h4 style="color: whitesmoke">Gedung Putri</h4>
                    </div>
                </figcaption>
            </figure>
        </div>
        <div class="gallery-item col-lg-3 col-md-3 ecommerce programming">
            <figure class="picture-item img-block shine-effect image-zoom port-v2">
                <img src="{{ asset('assets/compro/img/compro/Fasilitas PPM/basement.jpg') }}" alt="images description">
                <figcaption>
                    <div class="link-box">
                        <h4 style="color: whitesmoke">Parkir Basement</h4>
                    </div>
                </figcaption>
            </figure>
        </div>
        <div class="gallery-item col-lg-3 col-md-3 programming ecommerce">
            <figure class="picture-item img-block shine-effect image-zoom port-v2">
                <img src="{{ asset('assets/compro/img/compro/Fasilitas PPM/ruang makan.jpg') }}" alt="images description">
                <figcaption>
                    <div class="link-box">
                        <h4 style="color: whitesmoke">Ruang Makan</h4>
                    </div>
                </figcaption>
            </figure>
        </div>
        <div class="gallery-item col-lg-3 col-md-3 ecommerce ui">
            <figure class="picture-item img-block shine-effect image-zoom port-v2">
                <img src="{{ asset('assets/compro/img/compro/Fasilitas PPM/pantry.jpg') }}" alt="images description">
                <figcaption>
                    <div class="link-box">
                        <h4 style="color: whitesmoke">Pantry</h4>
                    </div>
                </figcaption>
            </figure>
        </div>
        <div class="gallery-item col-lg-3 col-md-3 ecommerce ui">
            <figure class="picture-item img-block shine-effect image-zoom port-v2">
                <img src="{{ asset('assets/compro/img/compro/Fasilitas PPM/aula.jpg') }}" alt="images description">
                <figcaption>
                    <div class="link-box">
                        <h4 style="color: whitesmoke">Aula (Greenland)</h4>
                    </div>
                </figcaption>
            </figure>
        </div>
        <div class="gallery-item col-lg-3 col-md-3 ecommerce ui">
            <figure class="picture-item img-block shine-effect image-zoom port-v2">
                <img src="{{ asset('assets/compro/img/compro/Fasilitas PPM/olah raga.jpg') }}" alt="images description">
                <figcaption>
                    <div class="link-box">
                        <h4 style="color: whitesmoke">Area Olahraga</h4>
                    </div>
                </figcaption>
            </figure>
        </div>
        <div class="gallery-item col-lg-3 col-md-3 programming ecommerce">
            <figure class="picture-item img-block shine-effect image-zoom port-v2">
                <img src="{{ asset('assets/compro/img/compro/Fasilitas PPM/router.jpg') }}" alt="images description">
                <figcaption>
                    <div class="link-box">
                        <h4 style="color: whitesmoke">Wi-fi</h4>
                    </div>
                </figcaption>
            </figure>
        </div>
        <div class="gallery-item col-lg-3 col-md-3 ecommerce ui">
            <figure class="picture-item img-block shine-effect image-zoom port-v2">
                <img src="{{ asset('assets/compro/img/compro/Fasilitas PPM/mesin cuci.jpg') }}" alt="images description">
                <figcaption>
                    <div class="link-box">
                        <h4 style="color: whitesmoke">Mesin Cuci</h4>
                    </div>
                </figcaption>
            </figure>
        </div>
        <div class="gallery-item col-lg-3 col-md-3 ecommerce ui">
            <figure class="picture-item img-block shine-effect image-zoom port-v2">
                <img src="{{ asset('assets/compro/img/compro/Fasilitas PPM/RO.jpg') }}" alt="images description">
                <figcaption>
                    <div class="link-box">
                        <h4 style="color: whitesmoke">Air Minum</h4>
                    </div>
                </figcaption>
            </figure>
        </div>
        <div class="gallery-item col-lg-3 col-md-3 ecommerce ui">
            <figure class="picture-item img-block shine-effect image-zoom port-v2">
                <img src="{{ asset('assets/compro/img/compro/Fasilitas PPM/kamar mandi.jpg') }}" alt="images description">
                <figcaption>
                    <div class="link-box">
                        <h4 style="color: whitesmoke">Kamar Mandi</h4>
                    </div>
                </figcaption>
            </figure>
        </div>
    </div>
</section>
