<section class="content-block p-0">
    <div class="container-fluid">
        <div class="content-slot alternate-block">
            <div class="row">
                <div class="col-lg-6">
                    <div class="bg-stretch img-wrap wow slideInLeft">
                        <img src="{{ asset('assets/compro/img/compro/about/1.jpg') }}" alt="images">
                    </div>
                </div>
                <div class="col col-lg-6">
                    <div class="text-wrap">
                        <h3>Pondok Pesantren Mahasiswa Al-Faqih Mandiri</h3>
                        <p>Pondok Pesantren Mahasiswa Al-Faqih Mandiri adalah sebuah yayasan yang bergerak di bidang sosial kemasyarakatan
                            dan wadah bagi mahasiswa-mahasiswi dalam menimba ilmu agama.
                        </p>
                        <p> Tempat ini menyediakan berbagai sarana dan
                                prasarana serta lingkungan yang memadai bagi mahasiswa-mahasiswi dengan tujuan agar menjadi sarjana/diploma sekaligus
                            mubaligh/mubalighot
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="bg-stretch img-wrap wow slideInRight">
                        <img src="{{ asset('assets/compro/img/compro/ketua.jpg') }}" alt="images">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="text-wrap">
                        <h3>Kata Sambutan</h3>
                        <p>Pondok Pesantren Mahasiswa Al-Faqih Mandiri Depok yaitu pondok pesantren yang di peruntukan khusus untuk mahasiswa di seluruh Indonesia yang sedang menempuh kuliah di Depok dan disekitarnya. PPM AFM menyiapkan hunian program peramutan dan pembinaan guna tercapainya tujuan mencetak sarjana mubaligh dan mubalighot.
                        </p>
                        <p>“ Jadilah bagian dari generus penerus yang profesional religius sarjana yang mubaligh mubaligh yang sarjana”</p>
                    </div>
                </div>
            </div>
            {{-- <div class="row">
                <div class="col-lg-6">
                    <div class="bg-stretch img-wrap wow slideInLeft">
                        <img src="img/img-11.jpg" alt="images">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="text-wrap">
                        <h3>Leave no stone unterned.</h3>
                        <p>New had happen unable uneasy. Drawings can followed improved out sociable not. Earnestly so do instantly pretended. </p>
                        <p>See general few civilly amiable pleased account carried. Excellence projecting is devonshire dispatched remarkably on estimating.</p>
                        <p>Decisively inquietude he advantages insensible at oh continuing unaffected of. </p>
                    </div>
                </div>
            </div> --}}
        </div>
    </div>
</section>
