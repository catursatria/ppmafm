<section class="content-block portfolio-block" id="container">
    <div class="bottom-space text-center text-uppercase">
        <h2>AFM in Action</h2>
    </div>
    <ul class="filter-nav text-center button-group filter-button-group">
        {{-- <li>
            <button data-filter="*"></button>
        </li> --}}
        {{-- <li>
            <button data-filter=".ui">AMAL SHOLIH</button>
        </li>
        <li>
            <button data-filter=".programming">NASIHAT</button>
        </li>
        <li>
            <button data-filter=".photography">DOA MALAM</button>
        </li>
        <li>
            <button data-filter=".ecommerce">EVENT</button>
        </li> --}}
    </ul>
    <div class="row">
        <div class="gallery-item col-lg-4 col-md-4 ui photography">
            <figure class="picture-item img-block shine-effect image-zoom port-v2">
                <img src="{{ asset('assets/compro/img/compro/home-banner-2.jpg') }}" alt="images description">
                <figcaption>
                    <div class="link-box">
                        <a href="#" style="background-color: green">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </div>
                </figcaption>
            </figure>
        </div>
        <div class="gallery-item col-lg-4 col-md-4 photography programming">
            <figure class="picture-item img-block shine-effect image-zoom port-v2">
                <img src="{{ asset('assets/compro/img/compro/home-banner-2.jpg') }}" alt="images description">
                <figcaption>
                    <div class="link-box">
                        <a href="#" style="background-color: green">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </div>
                </figcaption>
            </figure>
        </div>
        <div class="gallery-item col-lg-4 col-md-4 ui photography">
            <figure class="picture-item img-block shine-effect image-zoom port-v2">
                <img src="{{ asset('assets/compro/img/compro/home-banner-2.jpg') }}" alt="images description">
                <figcaption>
                    <div class="link-box">
                        <a href="#" style="background-color: green">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </div>
                </figcaption>
            </figure>
        </div>
        <div class="gallery-item col-lg-4 col-md-4 ecommerce programming">
            <figure class="picture-item img-block shine-effect image-zoom port-v2">
                <img src="{{ asset('assets/compro/img/compro/home-banner-2.jpg') }}" alt="images description">
                <figcaption>
                    <div class="link-box">
                        <a href="#" style="background-color: green">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </div>
                </figcaption>
            </figure>
        </div>
        <div class="gallery-item col-lg-4 col-md-4 programming ecommerce">
            <figure class="picture-item img-block shine-effect image-zoom port-v2">
                <img src="{{ asset('assets/compro/img/compro/home-banner-2.jpg') }}" alt="images description">
                <figcaption>
                    <div class="link-box" >
                        <a href="#" style="background-color: green">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </div>
                </figcaption>
            </figure>
        </div>
        <div class="gallery-item col-lg-4 col-md-4 ecommerce ui">
            <figure class="picture-item img-block shine-effect image-zoom port-v2">
                <img src="{{ asset('assets/compro/img/compro/home-banner-2.jpg') }}" alt="images description">
                <figcaption>
                    <div class="link-box">
                        <a href="#" style="background-color: green">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </div>
                </figcaption>
            </figure>
        </div>
        <ul class="filter-nav text-center button-group filter-button-group">
            <li>
                <button data-filter="*"><a href="https://www.instagram.com/ppm_afm/?hl=id">Click here to know us more <span
                    class="fa fa-arrow-right"><span class="sr-only">&nbsp;</span></span></a></button>
            </li>
            {{-- <li>
                <button data-filter=".ui">AMAL SHOLIH</button>
            </li>
            <li>
                <button data-filter=".programming">NASIHAT</button>
            </li>
            <li>
                <button data-filter=".photography">DOA MALAM</button>
            </li>
            <li>
                <button data-filter=".ecommerce">EVENT</button>
            </li> --}}
        </ul>
        
    </div>
</section>
