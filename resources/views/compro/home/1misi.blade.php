<section class="content-block">
    <div class="container">
        <div class="row multiple-row v-align-row">
            <div class="col-lg-4 col-md-6">
                <div class="col-wrap">
                    <div class="block-heading">
                        {{-- <h3 class="block-top-heading">WHY WE EXIST</h3> --}}
                        <h2 class="block-main-heading">MISI</h2>
                        <span class="block-sub-heading">What we believe we can do</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="col-wrap">
                    <div class="ico-box bg-gray-light has-radius-medium">
                        <div class="icon">
                            <i class="si si-home"></i>

                            {{-- <span class="custom-icon-pen-tool"><span class="sr-only">&amp;</span></span> --}}
                        </div>
                        {{-- <h4 class="content-title"><a href="#">PIXEL PERFECT</a></h4> --}}
                        <div class="des">
                            <p>Menyediakan sarana harian dan lingkungan yang nyaman sebagai tempat tinggal, belajar & mengembangkan kepribadian</p>
                        </div>
                        <div class="link-holder">
                            {{-- <a class="link-more" href="#">LEARN MORE</a> --}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="col-wrap">
                    <div class="ico-box bg-gray-light has-radius-medium">
                        <div class="icon">
                            <i class="si si-key"></i>
                            {{-- <span class="custom-icon-vector"><span class="sr-only">&amp;</span></span> --}}
                        </div>
                        {{-- <h4 class="content-title"><a href="#">VECTOR SHAPES</a></h4> --}}
                        <div class="des">
                            <p>Memfasilitasi mahasiswa untuk mencari dan mengamalkan ilmu agama dan dunia sebanyak-banyaknya</p>
                        </div>
                        <div class="link-holder">
                            {{-- <a class="link-more" href="#">LEARN MORE</a> --}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="col-wrap">
                    <div class="ico-box bg-gray-light has-radius-medium">
                        <div class="icon">
                            <i class="si si-globe-alt"></i>
                            {{-- <span class="custom-icon-font-design"><span class="sr-only">&amp;</span></span> --}}
                        </div>
                        {{-- <h4 class="content-title"><a href="#">GOOGLE FONTS</a></h4> --}}
                        <div class="des">
                            <p>Memberi wawasan, pemahaman, dan pelatihan tentang pendidikan karakter</p><br><br>
                        </div>
                        <div class="link-holder">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="col-wrap">
                    <div class="ico-box bg-gray-light has-radius-medium">
                        <div class="icon">
                            <i class="si si-directions"></i>
                            {{-- <span class="custom-icon-layers"><span class="sr-only">&amp;</span></span> --}}
                        </div>
                        {{-- <h4 class="content-title"><a href="#">EASY LAYERS</a></h4> --}}
                        <div class="des">
                            <p>Memberikan wawasan dan pemahaman tentang nilai-nilai sosial, budaya, politik, ekonomi, keamanan, dan perjuangan Qur'an Hadits</p>
                        </div>
                        <div class="link-holder">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="col-wrap">
                    <div class="ico-box bg-gray-light has-radius-medium">
                        <div class="icon">
                            {{-- <span class="custom-icon-list"><span class="sr-only">&amp;</span></span> --}}
                            <i class="si si-graduation"></i>
                        </div>
                        {{-- <h4 class="content-title"><a href="#">BOOTSTRAP GRID</a></h4> --}}
                        <div class="des">
                            <p>Memberikan wawasan, pemahaman, dan pelatiihan sebagai Mubaligh Sarjana</p><br><br>
                        </div>
                        <div class="link-holder">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
