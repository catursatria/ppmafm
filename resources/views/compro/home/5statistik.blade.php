<section class="content-block count-block text-center p-0 parallax" data-stellar-background-ratio="0.55">
    <div class="container-fluid">
        <div class="row no-gutters">
            <div class="col-sm-6 col-lg-3">
                <div class="col-wrap">
                    <div class="icon">
                        <i class="fa fa-graduation-cap" style="color: #c1c5cd"></i>
                        {{-- <span class="custom-icon-projects"></span> --}}
                    </div>
                    <h3 class="number">5</h3>
                    <div class="text text-uppercase">Dewan Guru</div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="col-wrap">
                    <div class="icon">
                        <span class="icon-users-1"></span>
                    </div>
                    <h3 class="number">98</h3>
                    <div class="text text-uppercase">TOTAL SANTRI</div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="col-wrap">
                    <div class="icon">
                        <i class="si si-user"></i>
                        {{-- <span class="custom-icon-award"></span> --}}
                    </div>
                    <h3 class="number">45</h3>
                    <div class="text text-uppercase">SANTRI PUTRA</div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="col-wrap">
                    <div class="icon">
                        <i class="si si-user-female"></i>
                        {{-- <span class="custom-icon-celebrate"></span> --}}
                    </div>
                    <h3 class="number">53</h3>
                    <div class="text text-uppercase">SANTRI PUTRI</div>
                </div>
            </div>
        </div>
    </div>
</section>
