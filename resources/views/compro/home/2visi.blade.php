<section class="content-block quotation-block2 black-overlay-6 parallax" data-stellar-background-ratio="0.55">
    <div class="container">
        <div class="inner-wrapper">
            <h3 class="block-top-heading text-white">Our VISION</h3>
            <h2 class="text-white">Muballigh yang Sarjana, Sarjana yang Muballigh</h2>
            <div class="btn-container">
                {{-- <a href="#" class="btn btn-primary has-radius-small">GET QUOTATION</a> --}}
            </div>
        </div>
    </div>
</section>
