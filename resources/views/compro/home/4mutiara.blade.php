
<section id="work-section" class="content-block work-block">
    <div class="bg-stretch">
        <img src="{{ asset('assets/compro/img/compro/home-banner-3.jpg') }}" alt="">
    </div>
    <div class="container">
        <div class="block-heading bottom-space text-center">
            {{-- <h3 class="block-top-heading" style="color: wheat">WORK PLACE</h3> --}}
            <h2 class="block-main-heading" style="color: wheat">HIKMAH BERCERITA</h2>
            <span class="block-sub-heading" style="color: wheat">Hikmah-hikmah berdasarkan dalil yang kita kaji</span>
            <div class="divider"><img src="{{ asset('assets/compro/img/divider.png') }}" alt="images description"></div>
        </div>
        <div class="description text-center container-md">
            <h2 style="color: white">يٰٓاَيُّهَا الَّذِيْنَ اٰمَنُوْٓا اِنْ تَنْصُرُوا اللّٰهَ يَنْصُرْكُمْ وَيُثَبِّتْ اَقْدَامَكُمْ</h2>
        </div>
        <div class="description text-center container-md">
            <p style="color: white">Wahai orang-orang yang beriman! Jika kamu menolong (agama) Allah, niscaya Dia akan menolongmu dan meneguhkan kedudukanmu.
            </p>
        </div>
    </div>
</section>
