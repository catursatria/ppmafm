<section class="content-block">
    <div class="container">
        <div class="row multiple-row v-align-row">
            <div class="col-md-6 col-lg-4">
                <div class="col-wrap">
                    <div class="block-heading">
                        <h3 class="block-top-heading">RECENTLY FROM</h3>
                        <h2 class="block-main-heading">THE JOURNAL</h2>
                        <span class="block-sub-heading">Blogging like no tomorrow.</span>
                        <div class="divider"><img src="{{ asset('assets/compro/img/divider.png') }}"
                                alt="images description"></div>
                        <div class="post-meta clearfix">
                            <div class="post-link-holder">
                                <a href="{{ route('berita.list') }}">View More News <span
                                        class="fa fa-arrow-right"><span class="sr-only">&nbsp;</span></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @foreach ($news as $item)
                <div class="col-md-6 col-lg-4">
                    <div class="col-wrap">
                        <div class="post-grid reverse-grid">
                            <div class="img-block post-img">
                                <a href="{{ route('berita.show', $item->id) }}"><img
                                        src="{{ asset('assets/dashboards/images/news/' . $item->foto()) }}"
                                        alt="images"></a>
                                <time class="post-date"
                                    datetime="2016-10-10">{{ $item->tanggal_publish->format('d M Y') }}</time>
                            </div>
                            <div class="post-text-block bg-gray-light">
                                <strong class="content-title mb-0"><a href="#">{{ $item->judul }}</a></strong>
                                <span class="content-sub-title">{{ $item->tanggal_publish->format('D,d M Y') }}</span>
                                <p>{{ $item->orientasi }}</p>
                                <div class="post-meta clearfix">
                                    <div class="post-link-holder">
                                        <a href="{{ route('berita.show', $item->id) }}">Read Story <span
                                                class="fa fa-arrow-right"><span class="sr-only">&nbsp;</span></span></a>
                                    </div>
                                    {{-- <div class="post-social text-right">
                                        <ul class="social-network social-small">
                                            <li><a href="#"><span class="icon-facebook"><span
                                                            class="sr-only">&nbsp;</span></span></a></li>
                                            <li><a href="#"><span class="icon-twitter"><span
                                                            class="sr-only">&nbsp;</span></span></a></li>
                                        </ul>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
