<div class="vertical-menu">
    <div class="h-100">

        <div class="user-wid text-center py-4">
            <div class="user-img">
                <img src="{{ asset('assets/dashboards/images/users/avatar-2.jpg')}}" alt="" class="avatar-md mx-auto rounded-circle">
            </div>

            <div class="mt-3">

                <a href="#" class="text-dark fw-medium font-size-16">Catur Satria</a>
                <p class="text-body mt-1 mb-0 font-size-13">Santri</p>

            </div>
        </div>

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Menu</li>

                <li>
                    <a href="javascript: void(0);" class="waves-effect">
                        <i class="mdi mdi-airplay"></i><span
                            class="badge rounded-pill bg-info float-end">2</span>
                        <span>Dashboard</span>
                    </a>
                    {{-- <ul class="sub-menu" aria-expanded="false">
                        <li><a href="index.html">Dashboard 1</a></li>
                        <li><a href="index-2.html">Dashboard 2</a></li>
                    </ul> --}}
                </li>

                <li class="{{ (request()->is('santri/datasantri*')) ? 'mm-active' : '' }}">
                    <a href="{{ route('santri.index') }}" class=" waves-effect {{ (request()->is('santri/datasantri*')) ? 'active' : '' }}">
                        <i class="mdi mdi-table-headers-eye"></i>
                        <span>Data Santri</span>
                    </a>
                </li>

                <li class="{{ (request()->is('poin-santri*')) ? 'mm-active' : '' }}">
                    <a href="{{ route('poin.index') }}" class=" waves-effect" {{ (request()->is('poin-santri*')) ? 'active' : '' }}>
                        <i class="mdi mdi-table-headers-eye"></i>
                        <span>Monitor Poin</span>
                    </a>
                </li>

                <li>
                    <a href="calendar.html" class=" waves-effect">
                        <i class="mdi mdi-equalizer-outline"></i>
                        <span>Monitor Kuantitas Materi</span>
                    </a>
                </li>

            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
