<!-- jQuery Library -->
<script src="{{ asset('assets/compro/vendors/jquery/jquery-2.1.4.min.js')}}"></script>
<!-- Vendor Scripts -->
<script src="{{ asset('assets/compro/vendors/tether/dist/js/tether.min.js')}}"></script>
<script src="{{ asset('assets/compro/vendors/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('assets/compro/vendors/stellar/jquery.stellar.min.js')}}"></script>
<script src="{{ asset('assets/compro/vendors/isotope/javascripts/isotope.pkgd.min.js')}}"></script>
<script src="{{ asset('assets/compro/vendors/isotope/javascripts/packery-mode.pkgd.js')}}"></script>
<script src="{{ asset('assets/compro/vendors/owl-carousel/dist/owl.carousel.min.js')}}"></script>
<script src="{{ asset('assets/compro/vendors/waypoint/waypoints.min.js')}}"></script>
<script src="{{ asset('assets/compro/vendors/counter-up/jquery.counterup.min.js')}}"></script>
<script src="{{ asset('assets/compro/vendors/fancyBox/source/jquery.fancybox.pack.js')}}"></script>
<script src="{{ asset('assets/compro/vendors/fancyBox/source/helpers/jquery.fancybox-thumbs.js')}}"></script>
<script src="{{ asset('assets/compro/vendors/image-stretcher-master/image-stretcher.js')}}"></script>
<script src="{{ asset('assets/compro/vendors/wow/wow.min.js')}}"></script>
<script src="{{ asset('assets/compro/vendors/rateyo/jquery.rateyo.min.js')}}"></script>
<script src="{{ asset('assets/compro/vendors/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}"></script>
<script src="{{ asset('assets/compro/vendors/bootstrap-slider-master/src/js/bootstrap-slider.js')}}"></script>
<script src="{{ asset('assets/compro/vendors/bootstrap-select/dist/js/bootstrap-select.min.js')}}"></script>
<script src="{{ asset('assets/compro/js/mega-menu.js')}}"></script>
<script src="{{ asset('assets/compro/vendors/retina/retina.min.js')}}"></script>
<!-- Custom Script -->
<script src="{{ asset('assets/compro/js/jquery.main.js')}}"></script>
<!-- REVOLUTION JS FILES -->
<script type="text/javascript" src="{{ asset('assets/compro/vendors/rev-slider/revolution/js/jquery.themepunch.tools.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/compro/vendors/rev-slider/revolution/js/jquery.themepunch.revolution.min.js')}}"></script>
<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="{{ asset('assets/compro/vendors/rev-slider/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/compro/vendors/rev-slider/revolution/js/extensions/revolution.extension.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/compro/vendors/rev-slider/revolution/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/compro/vendors/rev-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/compro/vendors/rev-slider/revolution/js/extensions/revolution.extension.migration.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/compro/vendors/rev-slider/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/compro/vendors/rev-slider/revolution/js/extensions/revolution.extension.parallax.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/compro/vendors/rev-slider/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/compro/vendors/rev-slider/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>
<!-- SNOW ADD ON -->
<script type="text/javascript" src="{{ asset('assets/compro/vendors/rev-slider/revolution-addons/snow/revolution.addon.snow.min.js')}}"></script>
<!-- Revolution Slider Script -->
<script src="{{ asset('assets/compro/js/revolution.js')}}"></script>
