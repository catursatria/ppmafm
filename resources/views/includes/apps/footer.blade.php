<!-- footer of the page -->
<footer class="footer footer-v1">
    <div class="content-block footer-main">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="footer-logo">
                        <img src="{{ asset('assets/compro/img/compro/logo-text-afm-putih.png') }}"  style="width: 50%" alt="ppmafm">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="footer-nav inline-nav text-center">
                        <ul>
                            {{-- <li><a href="#">Home</a></li>
                            <li><a href="#">About</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Contact</a></li> --}}
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 text-right">
                    <ul class="social-network with-text">
                        <li><a href="https://www.instagram.com/ppm_afm/?hl=id"><i class="fab fa-instagram"></i></a></li>
                        <li><a href="https://www.youtube.com/channel/UCBnBDmVo4t0isb5loPTdU-A"><i class="fab fa-youtube"></i> </a></li>
                        <li><a href="https://twitter.com/ppm_afm"><span class="icon-twitter"></span> </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom text-center">
        <div class="container">
            <p>Copyright 2021 - PPM Al-Faqih Mandiri </p>
        </div>
    </div>
</footer>
<!--/footer of the page -->
