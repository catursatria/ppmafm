    <!-- Favicon -->
    <link rel="icon" href="{{ asset('assets/compro/img/compro/logo-afm.png') }}" type="image/x-icon" />
    <!-- Font Icons -->
    <link media="all" rel="stylesheet" href="{{ asset('assets/compro/css/fonts/icomoon/icomoon.css') }}">
<!--     <link media="all" rel="stylesheet" href="{{ asset('assets/compro/css/fonts/roxine-font-icon/roxine-font.css') }}"> -->
    <link media="all" rel="stylesheet" href="{{ asset('assets/compro/vendors/font-awesome/css/font-awesome.css') }}">
    <!-- Vendors -->
    <link media="all" rel="stylesheet" href="{{ asset('assets/compro/vendors/owl-carousel/dist/assets/owl.carousel.min.css') }}">
    <link media="all" rel="stylesheet" href="{{ asset('assets/compro/vendors/owl-carousel/dist/assets/owl.theme.default.min.css') }}">
    <link media="all" rel="stylesheet" href="{{ asset('assets/compro/vendors/animate/animate.css') }}">
    <link media="all" rel="stylesheet" href="{{ asset('assets/compro/vendors/rateyo/jquery.rateyo.css') }}">
    <link media="all" rel="stylesheet" href="{{ asset('assets/compro/vendors/bootstrap-datepicker/css/bootstrap-datepicker.css') }}">
    <link media="all" rel="stylesheet" href="{{ asset('assets/compro/vendors/fancyBox/source/jquery.fancybox.css') }}">
    <link media="all" rel="stylesheet" href="{{ asset('assets/compro/vendors/fancyBox/source/helpers/jquery.fancybox-thumbs.css') }}">
    <!-- Bootstrap 4 -->
    <link media="all" rel="stylesheet" href="{{ asset('assets/compro/css/bootstrap.css') }}">
    <!-- Rev Slider -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/compro/vendors/rev-slider/revolution/css/settings.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/compro/vendors/rev-slider/revolution/css/layers.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/compro/vendors/rev-slider/revolution/css/navigation.css') }}">
    <!-- Theme CSS -->
    <link media="all" rel="stylesheet" href="{{ asset('assets/compro/css/main.css') }}">
    <!-- Custom CSS -->
    <link media="all" rel="stylesheet" href="{{ asset('assets/compro/css/custom.css') }}">
